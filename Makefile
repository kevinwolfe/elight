ifndef BASE
	BASE=/home/elight/eclipse_workspace
endif

export FW_GEN_PATH = $(BASE)/fw_generator/
export OPENOCD_DIR = $(BASE)/OpenOCD/
export RAMLOAD = ramload.sh
export FLASHPROG = flashprog.sh

FW_VERSION_INFO_FILE = ./src/fw_version_info.h
export FW_VERSION_MAJOR=$(shell grep "FW_VERSION_MAJOR" $(FW_VERSION_INFO_FILE) | awk '{print $$3}')
export FW_VERSION_MINOR=$(shell grep 'FW_VERSION_MINOR' $(FW_VERSION_INFO_FILE) | awk '{print $$3}')
export FW_VERSION_SUBMINOR=$(shell grep 'FW_VERSION_SUBMINOR' $(FW_VERSION_INFO_FILE) | awk '{print $$3}')
export FW_VERSION = $(shell printf "%02d-%02d-%02d" $(FW_VERSION_MAJOR) $(FW_VERSION_MINOR) $(FW_VERSION_SUBMINOR))

export WMSDK_BUNDLE = $(BASE)/wmsdk_bundle-3.2.12
export EXT_BLESTACK_PATH=$(BASE)/mrvl_blestack-41.0.0.14/mrvl_lestack_feature_packs/mc200/ble-fp2/mrvl_lestack
#export EXT_BLESTACK_PATH=$(BASE)/mrvl_blestack-41.0.0.14/mrvl_lestack
export SDK_PATH=$(WMSDK_BUNDLE)/bin/wmsdk
export BUILD_DIR = $(shell readlink -f .)/

export BOARDS_DIR = $(BUILD_DIR)/boards
export BOARD  = elight_board
export OS = $(shell uname)

AT=@

export ARCH-y=cortex-m3

PROJECT_NAME := elight-$(FW_VERSION)
export DST_NAME = $(PROJECT_NAME)

SRCS = \
	schedules.c					\
	api.c						\
	cloud.c						\
	main.c 						\
	ble.c 						\
	ble_elight_service.c 	\
	ble_gap_service.c 			\
	support_funcs.c				\
	io.c 						\
	timers_emb.c 				\
	light_control.c 			\
	uart.c 						\
	wifi_emb.c					\
	update_fw.c					\
	fw_keys_public.c 			\
	nvm.c 						\
	base64.c					\
	elight_board.c			\

EXTRACFLAGS += -DWM_IOT_PLATFORM=TRUE -DGATT_SERVER_SUPPORT=ENABLED -I $(SDK_PATH)/incl/sdk/crypto -I $(EXT_BLESTACK_PATH)/incl

ifeq ($(MAKECMDGOALS), sdk)
	export FORCE_CONFIG	= 1
endif

TOOLCHAIN_DIR := $(WMSDK_BUNDLE)/sample_apps/toolchains/gnu
include $(TOOLCHAIN_DIR)/targets.mk
include $(TOOLCHAIN_DIR)/rules.mk
export AXF2FW=$(WMSDK_BUNDLE)/wmsdk/tools/bin/Linux/axf2firmware
LDSCRIPT = elight.ld

run: 
	$(AT)echo "Loading Debug File: $(BIN_DIR)/"$(PROJECT_NAME).axf
	$(AT)sudo $(OPENOCD_DIR)/$(RAMLOAD) $(BIN_DIR)/$(PROJECT_NAME).axf
	
flash: 
	@echo mc200fw $(BUILD_DIR)/$(BIN_DIR)/$(PROJECT_NAME).bin > $(BUILD_DIR)/$(BIN_DIR)/batch_prog.txt
#	@echo ed_data $(BIN_DIR)/ed25519-data.bin >> $(BIN_DIR)/batch_prog.txt
	@echo boot2 $(BUILD_DIR)/boot2.bin >> $(BIN_DIR)/batch_prog.txt
	@echo wififw $(BUILD_DIR)/sd8777_uapsta.bin.xz >> $(BIN_DIR)/batch_prog.txt
	cd $(OPENOCD_DIR) && sudo $(OPENOCD_DIR)/$(FLASHPROG) -l $(BUILD_DIR)/layout_elight.txt -b $(BUILD_DIR)/$(BIN_DIR)/batch_prog.txt

encrypted:
	$(FW_GEN_PATH)/fw_generator $(BIN_DIR)/$(PROJECT_NAME).bin $(BIN_DIR)/$(PROJECT_NAME).encrypted.bin ./fw_keys_all.txt
	
sdk:
	$(AT)make -C $(WMSDK_BUNDLE) sdk_clean
	$(AT)make -C $(WMSDK_BUNDLE) $(BASE)/elight/elight_defconfig
	$(AT)make -C $(WMSDK_BUNDLE)

boot:
	$(AT)make -C $(BUILD_DIR)/bootloader
	$(AT)cp $(BUILD_DIR)/bootloader/bin/boot2.bin .