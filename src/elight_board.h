#ifndef _BOARD_FIXED_H_
#define _BOARD_FIXED_H_

	#include "board.h"

	I2C_ID_Type board_mfi_i2c_port_id();
	int board_console_uart_id(void);

#endif
