#ifndef _ELIGHT_SERVICE_H_
#define _ELIGHT_SERVICE_H_

	#include <mrvlstack_config.h>
	#include "main_gatt_api.h"
	
	#define UINT16_TO_BYTES(n)        ((uint8_t) (n)), ((uint8_t)((n) >> 8))
	
	// Elight UUIDs
    #define UUID_ELIGHT_BASE_0                  0x64
    #define UUID_ELIGHT_BASE_1                  0x85
    #define UUID_ELIGHT_BASE_2                  0x3c
    #define UUID_ELIGHT_BASE_3                  0x3e
    #define UUID_ELIGHT_BASE_4                  0x6b
    #define UUID_ELIGHT_BASE_5                  0x1e
    #define UUID_ELIGHT_BASE_6                  0x55
    #define UUID_ELIGHT_BASE_7                  0xaf
    #define UUID_ELIGHT_BASE_8                  0xa2
    #define UUID_ELIGHT_BASE_9                  0x4f    
    #define UUID_ELIGHT_BASE_a                  0x8c
    #define UUID_ELIGHT_BASE_b                  0x3b
	#define UUID_ELIGHT_BASE_c                  0x93
	#define UUID_ELIGHT_BASE_d                  0x77		// Nominally 0x77
    #define UUID_ELIGHT_BASE                    UUID_ELIGHT_BASE_0, UUID_ELIGHT_BASE_1, \
													UUID_ELIGHT_BASE_2, UUID_ELIGHT_BASE_3, \
													UUID_ELIGHT_BASE_4, UUID_ELIGHT_BASE_5, \
													UUID_ELIGHT_BASE_6, UUID_ELIGHT_BASE_7, \
													UUID_ELIGHT_BASE_8, UUID_ELIGHT_BASE_9, \
													UUID_ELIGHT_BASE_a, UUID_ELIGHT_BASE_b, \
													UUID_ELIGHT_BASE_c, UUID_ELIGHT_BASE_d

	// Macro for building Elight UUIDs
	#define UUID_ELIGHT_BASE_BUILD(part)    	UINT16_TO_BYTES(part), UUID_ELIGHT_BASE
													
    #define UUID_ELIGHT_SERVICE                 UUID_ELIGHT_BASE_BUILD(0x0000)
	#define UUID_ELIGHT_DEVICE_INFO             UUID_ELIGHT_BASE_BUILD(0x0001)
	#define UUID_ELIGHT_AUTHENTICATE_CONNECTION UUID_ELIGHT_BASE_BUILD(0x0002)		// 0x24
	#define UUID_ELIGHT_LIGHT_STATE    			UUID_ELIGHT_BASE_BUILD(0x0003)		// 0x26
	#define UUID_ELIGHT_API_PROVISIONING_DATA	UUID_ELIGHT_BASE_BUILD(0x0004)		// 0x28
//	#define UUID_ELIGHT_WIFI_SSID    			UUID_ELIGHT_BASE_BUILD(0x0005)		// Depreciated
//	#define UUID_ELIGHT_WIFI_PASSPHRASE			UUID_ELIGHT_BASE_BUILD(0x0006)		// Depreciated
	#define UUID_ELIGHT_FIRMWARE_UPDATE_URL		UUID_ELIGHT_BASE_BUILD(0x0007)		// 0x2A
	#define UUID_ELIGHT_MFG_TOOLS				UUID_ELIGHT_BASE_BUILD(0x0008)		// 0x2C
	#define UUID_ELIGHT_WIFI_PROVISIONING_DATA	UUID_ELIGHT_BASE_BUILD(0x0009)
	#define UUID_ELIGHT_SSID_LIST				UUID_ELIGHT_BASE_BUILD(0x000A)

	// Elight Service Handles
	enum
	{
		ELIGHT_DEVICE_ACTION_WIFI_CONNECT				= 0x00,		// Depreciated.
		ELIGHT_DEVICE_ACTION_RESET_NVM					= 0x01,
		ELIGHT_DEVICE_ACTION_RESET_DEVICE				= 0x02,
		ELIGHT_DEVICE_ACTION_FIRMWARE_UPDATE			= 0x03,		// Depreciated.
		ELIGHT_DEVICE_ACTION_API_PROVISION				= 0x04,		// Depreciated.
		ELIGHT_DEVICE_ACTION_TEST_LED_OFF				= 0x10,
		ELIGHT_DEVICE_ACTION_TEST_LED_BLINK				= 0x11,
		ELIGHT_DEVICE_ACTION_TEST_LED_ON				= 0x12,
		ELIGHT_DEVICE_POWER_TEST_LED_OFF				= 0x13,
		ELIGHT_DEVICE_POWER_TEST_LED_BLINK				= 0x14,
		ELIGHT_DEVICE_POWER_TEST_LED_ON					= 0x15,
		ELIGHT_DEVICE_ACTION_MFG_TEST_DRIVE_20HZ		= 0x20,
		ELIGHT_DEVICE_ACTION_MFG_TEST_WIFI_CONNECT		= 0x21,
		ELIGHT_DEVICE_ACTION_MFG_TEST_RESET_SYS_TIME	= 0x22,
		ELIGHT_DEVICE_ACTION_OTA_TO_GOLDEN				= 0x30,
	};

	void elight_serv_init(void);

#endif //_DIS_SERVICE_H
