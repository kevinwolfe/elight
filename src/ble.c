#include <wmstdio.h>
#include <wm_os.h>
#include <mdev.h>
#include <mdev_uart.h>
#include <mdev_sdio.h>
#include <cli.h>
#include <wlan.h>
#include <partition.h>
#include <flash.h>
#include <wmstdio.h>
#include <stdlib.h>

#include "mrvlstack_config.h"
#include "main_gap_api.h"
#include "memory_manager.h"
#include "ble_buffer_pool.h"

#include "timer.h"
#include "ble_gap_service.h"
#include "ble_elight_service.h"

#include "main_gap_api.h"
#include "main_gap_declares.h"
#include "main_gatt_api.h"
#include "ble_service_conf.h"

#include "ble.h"
#include "uart.h"
#include "fw_version_info.h"
#include "uart.h"
#include "wifi_emb.h"
#include "cloud.h"

bool rf_device_initialized;

// Define DEBUG Level
#if ((defined MRVL_STACK_DEBUG_ENABLED) && (MRVL_STACK_DEBUG_ENABLED == TRUE))
	extern uint8 SRVC_MSG_level = ALL;
	uint32 HID_MSG_level = ALL;				// For Debug Purpose
#endif

/// MAX ATT MTU size. Application can choose one of the block pool to configure MAX MTU Size.
#define ATT_MAX_MTU_SIZE     (POOL_ID0_BUF1_SZ - L2CAP_MIN_OFFSET - sizeof(Msg_Hdr))

#define HOG_MOUSE_CIRCLE_RADIUS			20	// 20 Units
#define HOG_MOUSE_CIRCLE_NUM_OF_LOOP	4

// Memory chunk given to BLE Stack for buffer management
__attribute__ ((aligned(4))) static uint8  stackBufferPool[POOL_ID0_SZ + POOL_ID0_OVERHEAD];
static uint32 stackTrackBuf[POOL_ID0_BUF0_CNT + POOL_ID0_BUF1_CNT + POOL_ID0_BUF2_CNT + POOL_ID0_BUF3_CNT];

//tbt_discovery_params disc_params;
t_device_config_data			config;

// Advertising data types */
#define BLE_ADV_TYPE_FLAGS           0x01  // Flag bits
#define BLE_ADV_TYPE_16_UUID_PART    0x02  // Partial list of 16 bit UUIDs
#define BLE_ADV_TYPE_16_UUID         0x03  // Complete list of 16 bit UUIDs
#define BLE_ADV_TYPE_128_UUID_PART   0x06  // Partial list of 128 bit UUIDs
#define BLE_ADV_TYPE_128_UUID        0x07  // Complete list of 128 bit UUIDs
#define BLE_ADV_TYPE_SHORT_NAME      0x08  // Shortened local name
#define BLE_ADV_TYPE_LOCAL_NAME      0x09  // Complete local name
#define BLE_ADV_TYPE_TX_POWER        0x0A  // TX power level
#define BLE_ADV_TYPE_CONN_INTERVAL   0x12  // Slave preferred connection interval
#define BLE_ADV_TYPE_SIGNED_DATA     0x13  // Signed data
#define BLE_ADV_TYPE_16_SOLICIT      0x14  // Service solicitation list of 16 bit UUIDs
#define BLE_ADV_TYPE_128_SOLICIT     0x15  // Service solicitation list of 128 bit UUIDs
#define BLE_ADV_TYPE_SERVICE_DATA    0x16  // Service data
#define BLE_ADV_TYPE_PUBLIC_TARGET   0x17  // Public target address
#define BLE_ADV_TYPE_RANDOM_TARGET   0x18  // Random target address
#define BLE_ADV_TYPE_APPEARANCE      0x19  // Device appearance
#define BLE_ADV_TYPE_MANUFACTURER    0xFF  // Manufacturer specific data

#define BLE_FLAG_LE_LIMITED_DISC     0x01  // Limited discoverable flag
#define BLE_FLAG_LE_GENERAL_DISC     0x02  // General discoverable flag
#define BLE_FLAG_LE_BREDR_NOT_SUP    0x04  // BR/EDR not supported flag


//-----------------------------------------------------------------------------
# pragma pack (1)
typedef struct
{
	// flags
	const char flags_length;
	const char flags_type;
	const char flags_value;

	// services
	const char services_length;
	const char services_type;
	const char services_value[16];
} bt_advertisement_struct;

typedef struct
{
	// manufacturer specific data
	const char mfg_data_length;
	const char mfg_data_type;
	elight_mfg_data_struct mfg_data;

	// manufacturer specific data
	const char dev_name_length;
	const char dev_name_type;
	const char device_name_value[sizeof(BT_DEVICE_NAME)];
} bt_scan_response_struct;
# pragma pack ()

bt_advertisement_struct advertisement_data =
{
	// Advertisement Flgas
	.flags_length = 2,
	.flags_type = BLE_ADV_TYPE_FLAGS,
	.flags_value = BLE_FLAG_LE_GENERAL_DISC | BLE_FLAG_LE_BREDR_NOT_SUP,

	.services_length = 17,
	.services_type = BLE_ADV_TYPE_128_UUID,
	.services_value = {UUID_ELIGHT_SERVICE},
};

bt_scan_response_struct scan_response_data =
{
	// Elight specific data
	.mfg_data_length = sizeof(elight_mfg_data_struct) + 1,
	.mfg_data_type = BLE_ADV_TYPE_MANUFACTURER,
	.mfg_data.device_id = {0, 0, 0, 0, 0, 0},
	.mfg_data.network_state_flag = -1, //WIFI_STATE_FLAG_UNKNOWN_STATE,
	.mfg_data.network_error_flag = 0,
	.mfg_data.cloud_state_flag = -1, //CLOUD_STATE_FLAG_UNKNOWN_STATE,
	.mfg_data.cloud_error_flag = 0,
	.mfg_data.provisioning_state_flag = -1, //PROVISIONING_STATE_FLAG_UNKNOWN_STATE,
	.mfg_data.provisioning_error_flag = 0,
	.mfg_data.fw_revision = {FW_VERSION_MAJOR, FW_VERSION_MINOR, FW_VERSION_SUBMINOR},

	// device name
	.dev_name_length = sizeof(BT_DEVICE_NAME) + 1,
	.dev_name_type = BLE_ADV_TYPE_LOCAL_NAME,
	.device_name_value = BT_DEVICE_NAME,
};
elight_mfg_data_struct * elight_mfg_data_ptr = &scan_response_data.mfg_data;
static char physical_device_id[PHYSICAL_DEVICE_ID_SIZE];

bool restart_ble_advertisements = false;
bool bt_connected = false;

void init_physical_id(tbt_device_id ble_device_id);

//-----------------------------------------------------------------------------
void ble_advertise()
{

	config.len = sizeof(tble_advertising_params);
	config.type = BT_CONFIG_T_ADVERTISING_PARAMS;
	tble_advertising_params default_advt_params = {
		GAP_BLE_DEFAULT_ADVERTISING_INT_MIN, GAP_BLE_DEFAULT_ADVERTISING_INT_MAX,
		GAP_BLE_DEFAULT_ADVERTISING_TYPE, GAP_BLE_DEFAULT_OWN_ADDRESS_TYPE, 0,
		{0, 0, 0, 0, 0, 0}, GAP_BLE_DEFAULT_ADVERTISING_CHANNEL_MAP, GAP_BLE_DEFAULT_ADVERTISING_FILTER_POLICY };
	config.p.adv_params = default_advt_params;
	GAP_Config_Device(&config);

    // Enable Advertising
	config.type = BT_CONFIG_T_ADVERTISING_DATA;
	config.p.adv_data.length = sizeof(bt_advertisement_struct);
	config.len = sizeof(tbt_advertising_data_params) + config.p.adv_data.length;
	memcpy(config.p.adv_data.data, (char*)&advertisement_data, sizeof(bt_advertisement_struct));
	config.p.adv_data.scan_rsp_data = FALSE;
	GAP_Config_Device(&config);

	config.type = BT_CONFIG_T_ADVERTISING_DATA;
	config.p.adv_data.length = sizeof(bt_scan_response_struct);
	config.len = sizeof(tbt_advertising_data_params) + config.p.adv_data.length;
	memcpy(config.p.adv_data.data, (char*)&scan_response_data, sizeof(bt_scan_response_struct));
	config.p.adv_data.scan_rsp_data = TRUE;
	GAP_Config_Device(&config);
	
	config.len = 0x02;
	config.type = BT_CONFIG_T_BLE_DEVICE_MODE;
	config.p.mode = GAP_BLE_GENERAL_DISCOVERABLE | GAP_BLE_CONNECTABLE;
	GAP_Config_Device(&config);
}

//-----------------------------------------------------------------------------
static void ble_app_handler(t_bt_gap_event_id gap_event_id,t_bt_gap_events_data* gap_event_data)
{
	switch(gap_event_id)
	{
		case BT_ENABLE_COMPLETED_EVENT:
		{
			write_message("Bluetooth Initialized");
		
			init_physical_id(((t_bt_enable_complete_event_params*)gap_event_data)->device_id);
			gap_serv_init();				// GAP BLE Service Init
			elight_serv_init();			// Elight BLE Service Init

			tbt_security_property_data	sec_prop_data;
			sec_prop_data.bond_mode		= BT_LE_GENERAL_BONDING_MODE; 		// No persistance storage
			sec_prop_data.io_cap		= BT_SEC_CAP_NO_IO;           		// To enforce, Just works model
			sec_prop_data.key_size		= 0x10;
			sec_prop_data.sec_level		= BT_SEC_UNAUTHENTICATED_SECURITY_LEVEL;

			// Setting Security Properties
			config.len			= 	sizeof(tbt_security_property_data);
			config.type				= BT_CONFIG_T_SECURITY_PROPERTY_DATA;
			config.p.sec_prop_data	= sec_prop_data;

			GAP_Config_Device(&config);
			ble_advertise();

			rf_device_initialized = true;
			return;
		}
		
		case BT_GAP_SEC_BONDING_COMPLETE_EVT:
			break;
	
		case BT_GAP_CONNECTION_PARAMS_UPDATED_EVT:
			break;

		case BT_GAP_LINK_CONNECT_COMPLETE_EVT:
			write_message("BLE Connected");
			bt_connected = true;
			break;
		
		case BT_GAP_LINK_DISCONNECT_COMPLETE_EVT:
			write_message("BLE Disconnected");
			restart_ble_advertisements = true;
			bt_connected = false;
//			ble_advertise();
			break;
		
		case BT_GAP_SEC_ENCRYPT_STATE_CHANGE_EVT:
			break;

		default:
			break;
	}
}

//-----------------------------------------------------------------------------
void initialize_bluetooth()
{
	tblock_pool_config pool_config =
	{
		{{POOL_ID0_BUF0_REAL_SZ, POOL_ID0_BUF0_CNT,},
		{POOL_ID0_BUF1_REAL_SZ, POOL_ID0_BUF1_CNT,},
		{POOL_ID0_BUF2_REAL_SZ, POOL_ID0_BUF2_CNT,},
		{POOL_ID0_BUF3_REAL_SZ, POOL_ID0_BUF3_CNT}}
	};

	Stack_InitBufferPools(stackBufferPool, stackTrackBuf, &pool_config, ATT_MAX_MTU_SIZE);

	GAP_Enable_Bluetooth(ble_app_handler);
}

//-----------------------------------------------------------------------------
// Formula: MOD ((input * key_1) + key_2),256)
// Note: Key_1 must be odd
char simple_substitution_cypher(char input, char key_1, char key_2)
{
	return (((int)input * key_1) + key_2) & 0xFF;
}

//-----------------------------------------------------------------------------
void init_physical_id(tbt_device_id ble_device_id)
{
	int index;
	for (index = 0; index < PHYSICAL_DEVICE_ID_SIZE; index++)
		physical_device_id[index] = simple_substitution_cypher(ble_device_id.address[index], 57, index * 3);

	memcpy(elight_mfg_data_ptr->device_id, physical_device_id, PHYSICAL_DEVICE_ID_SIZE);

//	write_message("BLE Address: %.02x:%.02x:%.02x:%.02x:%.02x:%.02x", ble_device_id.address[5], ble_device_id.address[4], ble_device_id.address[3], ble_device_id.address[2], ble_device_id.address[1], ble_device_id.address[0]);
//	write_message("Physical ID: %.02x:%.02x:%.02x:%.02x:%.02x:%.02x", physical_device_id[5], physical_device_id[4], physical_device_id[3], physical_device_id[2], physical_device_id[1], physical_device_id[0]);
}


//-----------------------------------------------------------------------------
void update_bt_advert_network_state_flag(char new_state_flag)
{
	if (elight_mfg_data_ptr->network_state_flag != new_state_flag)
	{
		elight_mfg_data_ptr->network_state_flag = new_state_flag;
		restart_ble_advertisements = true;
	}
}

//-----------------------------------------------------------------------------
void update_bt_advert_network_error_flag(char new_error_flag)
{
	if (new_error_flag == WIFI_ERROR_FLAG_CLEAR_ERRORS)
		new_error_flag = 0;
	else
		new_error_flag |= elight_mfg_data_ptr->network_error_flag;

	if (elight_mfg_data_ptr->network_error_flag != new_error_flag)
	{
		elight_mfg_data_ptr->network_error_flag = new_error_flag;
		restart_ble_advertisements = true;
	}
}

//-----------------------------------------------------------------------------
void update_bt_advert_cloud_state_flag(char new_state_flag)
{
	if (elight_mfg_data_ptr->cloud_state_flag != new_state_flag)
	{
		elight_mfg_data_ptr->cloud_state_flag = new_state_flag;
		restart_ble_advertisements = true;
	}
}

//-----------------------------------------------------------------------------
void update_bt_advert_cloud_error_flag(char new_error_flag)
{
	if (new_error_flag == CLOUD_ERROR_FLAG_CLEAR_ERRORS)
		new_error_flag = 0;
	else
		new_error_flag |= elight_mfg_data_ptr->cloud_error_flag;

	if (elight_mfg_data_ptr->cloud_error_flag != new_error_flag)
	{
		elight_mfg_data_ptr->cloud_error_flag = new_error_flag;
		restart_ble_advertisements = true;
	}
}

//-----------------------------------------------------------------------------
void update_bt_advert_provisioning_state_flag(char new_state_flag)
{
	if (elight_mfg_data_ptr->provisioning_state_flag != new_state_flag)
	{
		elight_mfg_data_ptr->provisioning_state_flag = new_state_flag;
		restart_ble_advertisements = true;
	}
}

//-----------------------------------------------------------------------------
void update_bt_advert_provisioning_error_flag(char new_error_flag)
{
	if (new_error_flag == PROVISIONING_ERROR_FLAG_CLEAR_ERRORS)
		new_error_flag = 0;
	else
		new_error_flag |= elight_mfg_data_ptr->provisioning_error_flag;

	if (elight_mfg_data_ptr->provisioning_error_flag != new_error_flag)
	{
		elight_mfg_data_ptr->provisioning_error_flag = new_error_flag;
		restart_ble_advertisements = true;
	}
}

//************************************************************************************************
void check_ble_advertisement_state()
{

	// Need this check.  When Wifi is initializing (which happens before BT init),
	// the wifi driver attempts to set the BLE advertisement data.  If we don't punt here,
	// we get WsfBufAlloc errors
	if (!rf_device_initialized)
		return;

	if (restart_ble_advertisements && !bt_connected)
	{
		ble_advertise();
		restart_ble_advertisements = false;
	}
}
