#include "utils.h"
#include "support_funcs.h"
#include "ble_service_conf.h"
#include "ble_elight_service.h"
#include <main_gatt_api.h>
#include <wmcrypto.h>

#include "schedules.h"
#include "uart.h"
#include "wifi_emb.h"
#include "light_control.h"
#include "ble.h"
#include "cloud.h"
#include "io.h"
#include "update_fw.h"
#include "timers_emb.h"
#include "main.h"
#include "base64.h"
#include "api.h"

#define AUTH_KEY_SIZE								(16)						// Example: 5ed0b1637ed7a0db43ab529a383746
#define API_PROVISIONING_DATA_LENGTH				(MAX_API_ID_STR_SIZE + 1 + MAX_API_CONNECTION_TOKEN_STR_SIZE)		// +1 for the seperator

#define AUTH_NONCE_SIZE								(8)
#define AUTH_DATA_LENGTH							(AUTH_NONCE_SIZE + SHA256_BASE64_HASH_SIZE)

void create_new_authentication_key();
bool authenticate_connection(uint8_t * nonce, uint8_t * encrypted_signature);

static bool 			connection_authenticated	= false;
static uint8_t			authentication_key[AUTH_KEY_SIZE];

static tgatt_uuid128_char_decl_record			elight_char_device_info_declaration;
static tgatt_uuid128_charval_decl_record_std 	elight_char_device_info_value;
static tgatt_uuid128_char_decl_record			elight_char_auth_connection_declaration;
static tgatt_uuid128_charval_decl_record_std 	elight_char_auth_connection_value;
static tgatt_uuid128_char_decl_record			elight_char_light_state_declaration;
static tgatt_uuid128_charval_decl_record_std 	elight_char_light_state_value;
static tgatt_uuid128_char_decl_record			elight_char_api_provisioning_declaration;
static tgatt_uuid128_charval_decl_record_std 	elight_char_api_provisioning_value;
static tgatt_uuid128_char_decl_record			elight_char_firmware_update_url_declaration;
static tgatt_uuid128_charval_decl_record_std 	elight_char_firmware_update_url_value;
static tgatt_uuid128_char_decl_record			elight_char_mfg_tools_declaration;
static tgatt_uuid128_charval_decl_record_std 	elight_char_mfg_tools_value;
static tgatt_uuid128_char_decl_record			elight_char_wifi_provisioning_declaration;
static tgatt_uuid128_charval_decl_record_std 	elight_char_wifi_provisioning_value;
static tgatt_uuid128_char_decl_record			elight_char_ssid_list_declaration;
static tgatt_uuid128_charval_decl_record_std 	elight_char_ssid_list_value;

//-----------------------------------------------------------------------------
static tgatt_uuid128_service_record elight_service_record;
static tgatts_cb_result elight_service_handler(tconn_id conn_id, tgatt_event_ID req_type , tgatts_event_param *p_req_data);

//-----------------------------------------------------------------------------
static uint8 characteristic_temp_buffer[512];
static boolean elight_service_initialized = FALSE;
static int ssid_list_len = 0;
static int current_char_handle = -1;
static int current_char_write_len;

//-----------------------------------------------------------------------------
// ELIGHT BLE Service
// Service Definition
static tgatt_uuid128_service_record elight_service_record = {
	NULL, {&elight_char_device_info_declaration, GATT_PRIM_SERVICE_RECORD_UUID128, GATT_START_HANDLE},
	16,	UUID_DECL_PRIMARY_SERVICE, elight_service_handler, {UUID_ELIGHT_SERVICE}};

// Device Info Declaration & Value Characteristics
static tgatt_uuid128_char_decl_record elight_char_device_info_declaration = {
	{&elight_char_device_info_value, GATT_CHAR_DECL_RECORD_UUID128, GATT_AUTO_HANDLE}, UUID_DECL_CHARACTERISTIC,
	GATT_CH_PROP_READ, GATT_NEXT_HANDLE, {UUID_ELIGHT_DEVICE_INFO}};
static tgatt_uuid128_charval_decl_record_std elight_char_device_info_value = {
    {&elight_char_auth_connection_declaration, GATT_CHARVAL_DECL_RECORD_UUID128_STD, GATT_AUTO_HANDLE},
    {UUID_ELIGHT_DEVICE_INFO}, GATT_PERM_READ_NO_SECURITY_LEVEL};
	
// Authenticate Connection Declaration & Value Characteristics
static tgatt_uuid128_char_decl_record elight_char_auth_connection_declaration = {
	{&elight_char_auth_connection_value, GATT_CHAR_DECL_RECORD_UUID128, GATT_AUTO_HANDLE}, UUID_DECL_CHARACTERISTIC,
	GATT_CH_PROP_READ | GATT_CH_PROP_WRITE, GATT_NEXT_HANDLE, {UUID_ELIGHT_AUTHENTICATE_CONNECTION}};
static tgatt_uuid128_charval_decl_record_std elight_char_auth_connection_value = {
    {&elight_char_light_state_declaration, GATT_CHARVAL_DECL_RECORD_UUID128_STD, GATT_AUTO_HANDLE},
    {UUID_ELIGHT_AUTHENTICATE_CONNECTION}, GATT_PERM_READ_NO_SECURITY_LEVEL | GATT_PERM_WRITE_NO_SECURITY_LEVEL};
	
// Light State & Value Characteristics
static tgatt_uuid128_char_decl_record elight_char_light_state_declaration = {
	{&elight_char_light_state_value, GATT_CHAR_DECL_RECORD_UUID128, GATT_AUTO_HANDLE}, UUID_DECL_CHARACTERISTIC,
	GATT_CH_PROP_READ | GATT_CH_PROP_WRITE, GATT_NEXT_HANDLE, {UUID_ELIGHT_LIGHT_STATE}};
static tgatt_uuid128_charval_decl_record_std elight_char_light_state_value = {
    {&elight_char_api_provisioning_declaration, GATT_CHARVAL_DECL_RECORD_UUID128_STD, GATT_AUTO_HANDLE},
    {UUID_ELIGHT_LIGHT_STATE}, GATT_PERM_READ_NO_SECURITY_LEVEL | GATT_PERM_WRITE_NO_SECURITY_LEVEL};

// API Provisioning Data Declaration & Value Characteristics
static tgatt_uuid128_char_decl_record elight_char_api_provisioning_declaration = {
	{&elight_char_api_provisioning_value, GATT_CHAR_DECL_RECORD_UUID128, GATT_AUTO_HANDLE}, UUID_DECL_CHARACTERISTIC,
	GATT_CH_PROP_WRITE, GATT_NEXT_HANDLE, {UUID_ELIGHT_API_PROVISIONING_DATA}};
static tgatt_uuid128_charval_decl_record_std elight_char_api_provisioning_value = {
    {&elight_char_firmware_update_url_declaration, GATT_CHARVAL_DECL_RECORD_UUID128_STD, GATT_AUTO_HANDLE},
    {UUID_ELIGHT_API_PROVISIONING_DATA}, GATT_PERM_WRITE_NO_SECURITY_LEVEL};

// Firmware Update URL Declaration & Value Characteristics
static tgatt_uuid128_char_decl_record elight_char_firmware_update_url_declaration = {
	{&elight_char_firmware_update_url_value, GATT_CHAR_DECL_RECORD_UUID128, GATT_AUTO_HANDLE}, UUID_DECL_CHARACTERISTIC,
	GATT_CH_PROP_WRITE, GATT_NEXT_HANDLE, {UUID_ELIGHT_FIRMWARE_UPDATE_URL}};
static tgatt_uuid128_charval_decl_record_std elight_char_firmware_update_url_value = {
    {&elight_char_mfg_tools_declaration, GATT_CHARVAL_DECL_RECORD_UUID128_STD, GATT_AUTO_HANDLE},
    {UUID_ELIGHT_FIRMWARE_UPDATE_URL}, GATT_PERM_WRITE_NO_SECURITY_LEVEL};

// Manufacturing Tools Declaration & Value Characteristics
static tgatt_uuid128_char_decl_record elight_char_mfg_tools_declaration = {
	{&elight_char_mfg_tools_value, GATT_CHAR_DECL_RECORD_UUID128, GATT_AUTO_HANDLE}, UUID_DECL_CHARACTERISTIC,
	GATT_CH_PROP_READ | GATT_CH_PROP_WRITE | GATT_CH_PROP_EXTENDED, GATT_NEXT_HANDLE, {UUID_ELIGHT_MFG_TOOLS}};
static tgatt_uuid128_charval_decl_record_std elight_char_mfg_tools_value = {
    {&elight_char_wifi_provisioning_declaration, GATT_CHARVAL_DECL_RECORD_UUID128_STD, GATT_AUTO_HANDLE},
    {UUID_ELIGHT_MFG_TOOLS}, GATT_PERM_READ_NO_SECURITY_LEVEL | GATT_PERM_WRITE_NO_SECURITY_LEVEL};

// Wifi Provisioning Declaration & Value Characteristics
static tgatt_uuid128_char_decl_record elight_char_wifi_provisioning_declaration = {
	{&elight_char_wifi_provisioning_value, GATT_CHAR_DECL_RECORD_UUID128, GATT_AUTO_HANDLE}, UUID_DECL_CHARACTERISTIC,
	GATT_CH_PROP_WRITE, GATT_NEXT_HANDLE, {UUID_ELIGHT_WIFI_PROVISIONING_DATA}};
static tgatt_uuid128_charval_decl_record_std elight_char_wifi_provisioning_value = {
    {&elight_char_ssid_list_declaration, GATT_CHARVAL_DECL_RECORD_UUID128_STD, GATT_AUTO_HANDLE},
    {UUID_ELIGHT_WIFI_PROVISIONING_DATA}, GATT_PERM_WRITE_NO_SECURITY_LEVEL};
	
// SSID List Declaration & Value Characteristics
static tgatt_uuid128_char_decl_record elight_char_ssid_list_declaration = {
	{&elight_char_ssid_list_value, GATT_CHAR_DECL_RECORD_UUID128, GATT_AUTO_HANDLE}, UUID_DECL_CHARACTERISTIC,
	GATT_CH_PROP_READ, GATT_NEXT_HANDLE, {UUID_ELIGHT_SSID_LIST}};
static tgatt_uuid128_charval_decl_record_std elight_char_ssid_list_value = {
    {NULL, GATT_CHARVAL_DECL_RECORD_UUID128_STD, GATT_AUTO_HANDLE},
    {UUID_ELIGHT_SSID_LIST}, GATT_PERM_READ_NO_SECURITY_LEVEL};

//-----------------------------------------------------------------------------
void elight_serv_init(void)
{
	// Add the  service record to GATT Only once, as there can be only single instance for Device Info Service
	if (!elight_service_initialized)
	{
		// Create DIS Service
		GATTS_Add_Service((void *) &elight_service_record);
		elight_service_initialized = TRUE;
	}
	return;
}

//-----------------------------------------------------------------------------
// Service Request Handler Registered with Local Gatt Server
// This Callback function handles all the Remote Gatt Client Requests & Events of this service
static tgatts_cb_result elight_service_handler(tconn_id conn_id, tgatt_event_ID req_type, tgatts_event_param *p_req_data)
{
	tgatts_cb_result result;
	result.status = ATT_SUCCESS;
	static tgatts_rsp gatt_response;  
	gatt_response.handle = ((tgatts_req *)p_req_data)->handle;
	gatt_response.rsp_of = req_type;
	
	uint16 new_char_handle	= ((tgatts_req *)p_req_data)->handle;
	uint16 offset			= ((tgatts_req *)p_req_data)->offset;
	
	bool	changed_handles = false;
	
	//-----------------------------------------------/
	// Handle new connections
	if (req_type == GATT_EVENT_CONNECTED) 
	{
		current_char_handle = -1;
		create_new_authentication_key();
		connection_authenticated = !cloud_is_provisioned();
	}
	
	// Any time we change handles, reset our temp buffer
	if ((current_char_handle != new_char_handle) && (req_type != GATT_EVENT_EXEC_WRITE_REQ))
	{
		memset(characteristic_temp_buffer, 0, 512);
		current_char_handle = new_char_handle;
		changed_handles = true;
	}

	//-----------------------------------------------/
	// Handle Read Requests
	if (req_type == GATT_EVENT_READ_REQ) 
	{
		// Use our temp work buffer for storing our response
		gatt_response.p_val = characteristic_temp_buffer;
	
		// BLE Authentication Key Characteristic
		if (current_char_handle == elight_char_auth_connection_value.header.handle)
		{
			memcpy(gatt_response.p_val, authentication_key, AUTH_KEY_SIZE);
			gatt_response.val_len = AUTH_KEY_SIZE;
		}

		else if (connection_authenticated)
		{
			// Elight Device Info Characteristic
			if (current_char_handle == elight_char_device_info_value.header.handle)
			{
				memcpy(gatt_response.p_val, elight_mfg_data_ptr, sizeof(elight_mfg_data_struct));
				gatt_response.val_len = sizeof(elight_mfg_data_struct);
			}
		
			// Light State Characteristic
			else if (current_char_handle == elight_char_light_state_value.header.handle)
			{
				gatt_response.p_val[0] = (char)light_state.power_state;
				gatt_response.p_val[1] = (char)light_state.target_level;
				gatt_response.val_len = 2;
			}
		
			// Test Field Characteristic
			else if (current_char_handle == elight_char_mfg_tools_value.header.handle)
			{

				float mcu_temp = read_mcu_temp();
				gatt_response.p_val[0] = true_ac_power_frequency_hz;
				gatt_response.p_val[1] = (char)mcu_temp;
#if (!PRODUCTION_BUILD)
				int current_time = wmtime_time_get_posix();
				memcpy((void*)&gatt_response.p_val[2], (void*)&current_time, 4);

				int boot_count = get_bootcount();
				memcpy((void*)&gatt_response.p_val[6], (void*)&boot_count, 4);

				get_loopbacks((char*)&gatt_response.p_val[10], (char*)&gatt_response.p_val[11]);

				gatt_response.val_len = 12;
#else
				gatt_response.val_len = 2;
#endif

			}
		
			// SSID List Characteristic - Must handle LONG Reads
			else if (current_char_handle == elight_char_ssid_list_value.header.handle)
			{
				if (changed_handles)
				{
					ssid_list_len = copy_ssid_list((char*)gatt_response.p_val);
					gatt_response.p_val[ssid_list_len - 1] = 0;
				}

				gatt_response.p_val		+= offset;
				gatt_response.val_len	= ssid_list_len - offset;

//				write_message("Read Request - Offset: %i, Len: %i / %i", offset, ssid_list_len, gatt_response.val_len);
			}
		}
		
		// If not authenticated but we need to be...
		else if ((current_char_handle == elight_char_device_info_value.header.handle)			||
				 (current_char_handle == elight_char_light_state_value.header.handle)			||
				 (current_char_handle == elight_char_wifi_provisioning_value.header.handle)		||
				 (current_char_handle == elight_char_ssid_list_value.header.handle))
			result.status = ATT_INSUFF_AUTHEN;

		// All others - Reads not supported
		else if ((current_char_handle == elight_char_api_provisioning_value.header.handle)		||
				 (current_char_handle == elight_char_firmware_update_url_value.header.handle)	||
				 (current_char_handle == elight_char_wifi_provisioning_value.header.handle))
			result.status = ATT_READ_NOT_PERMIT;

		else
			result.status = ATT_INVALID_HANDLE;
			
		GATTS_Send_Rsp(conn_id, result.status, (tgatts_rsp *)&gatt_response); 
	}
	
	//-----------------------------------------------/
	// Handle Reception of new Write Data
	// Do not respond the received data, just store it for now
	if ((req_type == GATT_EVENT_PREP_WRITE_REQ) || (req_type == GATT_EVENT_WRITE_REQ) || (req_type == GATT_EVENT_WRITE_CMD))
	{
//		write_message("Receiving Write Data - Handle: %x", current_char_handle);
//		write_message("Offset: %i, Len: %i", p_req_data->request_param.offset, p_req_data->request_param.val_len);

		if (changed_handles)
			current_char_write_len = 0;

		if ((current_char_handle == elight_char_auth_connection_value.header.handle)		||
			(current_char_handle == elight_char_firmware_update_url_value.header.handle)	||
		    (current_char_handle == elight_char_api_provisioning_value.header.handle)		||
			(current_char_handle == elight_char_wifi_provisioning_value.header.handle)		||
			(current_char_handle == elight_char_light_state_value.header.handle)			||
			(current_char_handle == elight_char_mfg_tools_value.header.handle))
		{
			current_char_write_len	= MAX(current_char_write_len, p_req_data->request_param.offset + p_req_data->request_param.val_len);
			memcpy(characteristic_temp_buffer + p_req_data->request_param.offset, ((tgatts_req *)p_req_data)->p_value, p_req_data->request_param.val_len);

			gatt_response.offset	= p_req_data->request_param.offset;
			gatt_response.val_len	= p_req_data->request_param.val_len;
			gatt_response.p_val		= p_req_data->request_param.p_value;
			gatt_response.handle	= p_req_data->request_param.handle;        
		}

		if ((gatt_response.offset + p_req_data->request_param.val_len) > 512)
			write_message("BLE Write Error - Write Req too big!");
		else if (req_type == GATT_EVENT_PREP_WRITE_REQ)
			GATTS_Send_Rsp(conn_id, result.status, (tgatts_rsp *)&gatt_response); 
	}

	//-----------------------------------------------/
	// Execute Write Request
	if ((req_type == GATT_EVENT_EXEC_WRITE_REQ) || (req_type == GATT_EVENT_WRITE_REQ) || (req_type == GATT_EVENT_WRITE_CMD))
	{
//		write_message("Executing Write Request - Handle: %x", current_char_handle);
		
		// BLE Connection Authentication Characteristic
		if (current_char_handle == elight_char_auth_connection_value.header.handle)
		{
			if (current_char_write_len != AUTH_DATA_LENGTH)
				result.status = ATT_INVALID_ATTR_VALUE_LEN;
			else
			{
				connection_authenticated = authenticate_connection(characteristic_temp_buffer, characteristic_temp_buffer + AUTH_NONCE_SIZE);
				if (connection_authenticated)
					write_message("BLE Authenticated!");
				else
				{
					write_message("BLE Authentication failed.");
					write_message("\tReminder: Signing Key:\t%s", get_singing_key());

					result.status = ATT_INSUFF_AUTHEN;
				}
			}
		}

		// For all of our authenticated characteristics
		else if (connection_authenticated)
		{
			// Light State Characteristic
			if (current_char_handle == elight_char_light_state_value.header.handle)
			{
				light_state_struct light_vals = {							\
						.power_state = characteristic_temp_buffer[0], 		\
						.target_level = characteristic_temp_buffer[1], 		\
						.pulse_lighting = 0									};

				#if (CORDELIA_BOARD)
					configure_cordelia(light_vals.power_state, light_vals.target_level);
				#else
					set_lighting(light_vals);
				#endif
			}

			// API Provisioning Characteristic
			else if (current_char_handle == elight_char_api_provisioning_value.header.handle)
			{
				// If we're writing the last expected chunk, begin the provisioning
				if (current_char_write_len != API_PROVISIONING_DATA_LENGTH)
					result.status = ATT_INVALID_ATTR_VALUE_LEN;
				else
				{
					// Expecting the ID & Connection Token should be separated with an ASCII space
					char * api_id = (char*)characteristic_temp_buffer;
					char * api_connection_token = strchr((const char *)characteristic_temp_buffer, '.');
					if (api_connection_token != NULL)
					{
						*(api_connection_token++) = 0;	// Null Terminate the API ID & move the conn. token ptr forward 1
						register_new_cloud_params(api_id, api_connection_token);
					}
					else
						result.status = ATT_INSUFF_AUTHEN;
				}
			}

			// Firmware URL Characteristic
			else if (current_char_handle == elight_char_firmware_update_url_value.header.handle)
			{
				run_fw_update((char*)characteristic_temp_buffer);
			}

			// Manufacturing Tools Characteristic
			else if (current_char_handle == elight_char_mfg_tools_value.header.handle)
			{
//				write_message("MFG Tool Len: %i", current_char_write_len);
				if (current_char_write_len != 1)
					result.status = ATT_INVALID_ATTR_VALUE_LEN;
				else
				{
//					write_message("MFG Tool Action: %i", characteristic_temp_buffer[0]);
					switch (characteristic_temp_buffer[0])
					{
						case ELIGHT_DEVICE_ACTION_RESET_NVM:
							reset_nvm_data();
							break;

						case ELIGHT_DEVICE_ACTION_RESET_DEVICE:
							pm_reboot_soc();
							break;
#if (!PRODUCTION_BUILD)
						case ELIGHT_DEVICE_ACTION_TEST_LED_OFF:
							set_mfg_green_led(MFG_LED_OFF);
							break;
						case ELIGHT_DEVICE_ACTION_TEST_LED_ON:
							set_mfg_green_led(MFG_LED_ON);
							break;
						case ELIGHT_DEVICE_ACTION_TEST_LED_BLINK:
							set_mfg_green_led(MFG_LED_BLINK);
							break;

						case ELIGHT_DEVICE_POWER_TEST_LED_OFF:
							set_mfg_red_led(MFG_LED_OFF);
							break;
						case ELIGHT_DEVICE_POWER_TEST_LED_BLINK:
							set_mfg_red_led(MFG_LED_BLINK);
							break;
						case ELIGHT_DEVICE_POWER_TEST_LED_ON:
							set_mfg_red_led(MFG_LED_ON);
							break;

						case ELIGHT_DEVICE_ACTION_MFG_TEST_DRIVE_20HZ:
							drive_20hz();
							break;

						case ELIGHT_DEVICE_ACTION_MFG_TEST_RESET_SYS_TIME:
							wmtime_time_set_posix(0);
							force_time_sync();
							break;

						case ELIGHT_DEVICE_ACTION_MFG_TEST_WIFI_CONNECT:
							set_new_wifi_ssid("elight");
							set_new_wifi_passphrase("elight");
							wifi_connect_to_new_network();
							break;

						case ELIGHT_DEVICE_ACTION_OTA_TO_GOLDEN:
							reset_nvm_data();
							run_fw_update("bin.elight.co/manufacturing.bin");
							break;
#endif
						// Depreciated actions.  Do not fail the BLE write, just don't do anything
						case ELIGHT_DEVICE_ACTION_WIFI_CONNECT:
						case ELIGHT_DEVICE_ACTION_FIRMWARE_UPDATE:
						case ELIGHT_DEVICE_ACTION_API_PROVISION:
							break;

						default:
							result.status = ATT_REQ_NOT_SUPPORT;
							break;
					}
				}
			}

			// Wifi Provisioning Characteristic
			else if (current_char_handle == elight_char_wifi_provisioning_value.header.handle)
			{
				uint8_t decrypted_data[current_char_write_len];
				decrypt_buffer(characteristic_temp_buffer, decrypted_data, authentication_key, current_char_write_len);

				// Expecting the ID & Connection Token should be separated with an ASCII space
				char * ssid = (char*)decrypted_data;
				if ((strlen(ssid) <= MAX_SSID_NAME_SIZE) && (current_char_write_len - strlen(ssid)) > 0)
				{
					char * passphrase = ssid + strlen(ssid) + 1;
					if ((strlen(passphrase) <= MAX_SSID_PASSPHRASE_SIZE) && ((strlen(ssid) + strlen(passphrase) + 2) == current_char_write_len))
					{
						set_new_wifi_ssid(ssid);
						set_new_wifi_passphrase(passphrase);
						wifi_connect_to_new_network();
					}
				}
				else
					result.status = ATT_INSUFF_AUTHEN;
			}
		}
		// If not authenticated but we need to be...
		else if ((current_char_handle == elight_char_light_state_value.header.handle)			||
				 (current_char_handle == elight_char_api_provisioning_value.header.handle)		||
				 (current_char_handle == elight_char_mfg_tools_value.header.handle)				||
				 (current_char_handle == elight_char_firmware_update_url_value.header.handle)	||
				 (current_char_handle == elight_char_wifi_provisioning_value.header.handle))
		{
			write_message("Not Authenticated.  Ignoring Write Request");
			result.status = ATT_INSUFF_AUTHEN;
		}

		// All Else
		else if ((current_char_handle == elight_char_device_info_value.header.handle)			||
				 (current_char_handle == elight_char_ssid_list_value.header.handle))
 			result.status = ATT_WRITE_NOT_PERMIT;
		else
			result.status = ATT_INVALID_HANDLE;
			
		if (req_type == GATT_EVENT_WRITE_REQ)
			GATTS_Send_Rsp(conn_id, result.status, (tgatts_rsp *)&gatt_response); 
	}

	return result;
}

/*****************************************************************************/
void create_new_authentication_key()
{
	const uint8_t *msg[1];
	int temp_int;
	float temp_float;

	// Load up the auth key with some regularly changing values
	temp_int = wmtime_time_get_posix();		memcpy(authentication_key + 0, &temp_int, 4);
	temp_int = get_bootcount();				memcpy(authentication_key + 4, &temp_int, 4);
	temp_int = get_uptime();				memcpy(authentication_key + 8, &temp_int, 4);
	temp_float = read_mcu_temp();			memcpy(authentication_key + 12, &temp_float, 4);

	// Hash the auth key to jumble it up
	temp_int = AUTH_KEY_SIZE;
	msg[0] = (const uint8_t *)&authentication_key;
	mrvl_sha1_vector(1, msg, (const size_t *)&temp_int, authentication_key, AUTH_KEY_SIZE);
}


/*****************************************************************************/
// Returns true if the credentials check out
bool authenticate_connection(uint8_t * nonce, uint8_t * encrypted_signature)
{
	if (!cloud_is_provisioned())
	{
		write_message("Device not provisioned, allowing authenticated actions");
		return true;
	}

	uint8_t calculated_sig[SHA256_HASH_SIZE];						// 32 * 8 = 256 Bits
	uint8_t calculated_sig_base64[SHA256_BASE64_HASH_SIZE];
	mrvl_hmac_sha256((const uint8_t *)get_singing_key(), strlen((const char *)get_singing_key()), \
					 nonce, AUTH_NONCE_SIZE, 				\
					 calculated_sig, SHA256_HASH_SIZE);

	Base64encode((char*)calculated_sig_base64, (char*)calculated_sig, SHA256_HASH_SIZE);

	uint8_t calculated_sig_base64_encrypted[SHA256_BASE64_HASH_SIZE];
	encrypt_buffer(calculated_sig_base64, calculated_sig_base64_encrypted, authentication_key, SHA256_BASE64_HASH_SIZE);

	return !(memcmp(calculated_sig_base64_encrypted, encrypted_signature, SHA256_BASE64_HASH_SIZE));
}
