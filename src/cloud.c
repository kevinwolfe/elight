// Note, the following are defined by the wicentric make file (...\stack\build\common.mak)
// and also in settings.h (as included by wmcrypto.h).  By undefining them here, we suppress
// a GCC warning.  I suspect Marvell will fix this in the future, but until then...
#undef WMSDK_BUILD
#undef NO_DEV_RANDOM
#undef NO_PSK
#undef NO_FILESYSTEM
#undef FREERTOS
#undef CYASSL_LWIP

#include <wmstats.h>
#include "api.h"
#include "timers_emb.h"
#include "cloud.h"
#include "ble.h"
#include "uart.h"
#include "wifi_emb.h"
#include "jsonv2.h"
#include "support_funcs.h"
#include "main.h"
#include "io.h"

#define HTTP_GET_TIMEOUT_MS					(5 * 1000)
#define JSON_PARSER_MAX_TOKENS      		10
#define MAX_PUBNUB_CHANNEL_STR_SIZE			(36)	// Example: 7cbc43de-8513-4bdf-86d7-1c0639ebe87c
#define MAX_PUBNUB_SUBSCRIBE_KEY_STR_SIZE	(42)	// Example: sub-c-a07d07a6-1d3b-11e4-a9f2-02ee2ddab7fe
#define MAX_PUBNUB_AUTH_KEY_STR_SIZE		(36)	// Example: 222b329f-a295-4aa1-a13b-7467978fa287
#define PUBNUB_SESSION_DURATION_S			(150)
#define MAX_PROVISIONING_TIME_S				(120)

// Persistent store data struct
typedef struct {
	char pubnub_channel[MAX_PUBNUB_CHANNEL_STR_SIZE + 1];
	char pubnub_subscribe_key[MAX_PUBNUB_SUBSCRIBE_KEY_STR_SIZE + 1];
	char pubnub_auth_key[MAX_PUBNUB_AUTH_KEY_STR_SIZE + 1];
	char api_id[MAX_API_ID_STR_SIZE + 1];
	char api_signing_key[MAX_API_SIGNING_KEY_STR_SIZE + 1];
	char nvm_magic_key[ENCRYPTED_STORE_MAGIC_KEY_LEN];
} cloud_provisioned_params_struct;

static void set_cloud_state(enum cloud_state_enums new_state);
static int handle_pubnub_framing(char * msg);		// Returns number of bytes processed
static void handle_pubnub_message(char * msg);
static void open_pubnub_http_session();
static void issue_pubnub_get_request();
static void process_pubnub_response();
static void write_cloud_nvm_params();

static void set_api_signing_secret();
static void set_pubnub_params();

static void invalidate_pubnub_auth_key();
static bool pubnub_auth_key_invalidated();

static bool first_connection = false;
static volatile enum cloud_state_enums cloud_state = CLOUD_STATE_IDLE;
static char api_connection_token[MAX_API_CONNECTION_TOKEN_STR_SIZE + 1];
static volatile cloud_provisioned_params_struct provisioned_params;
static http_session_t pubnub_session;
static int pubnub_session_opened_timemark;
static int provisioning_start_time = -1;

static jsontok_t json_tokens[JSON_PARSER_MAX_TOKENS];
static jobj_t json_parser;

//*****************************************************************************
void initialize_cloud() {
	// Read Cloud Provisioning Data from NVM
	set_cloud_state(CLOUD_STATE_IDLE);

	char read_status = read_nvm_encrypted_value(NVM_NAME_CLOUD_PARAMS,
			(void*)&provisioned_params, sizeof(cloud_provisioned_params_struct),
			(void*)provisioned_params.nvm_magic_key);

	if (read_status == NVM_FAILURE)
	{
		memset((void*)&provisioned_params, 0, sizeof(cloud_provisioned_params_struct));
		update_bt_advert_provisioning_state_flag(PROVISIONING_STATE_FLAG_UNPROVISIONED);
	}

	check_cloud_provisioning();
}

//*****************************************************************************
// This function will be called at boot & whenever the WiFi cloud_state machine transitions to the Connected cloud_state
// The function should ensure that all cloud connections are closed and marked closed.
void reset_cloud_state_machine()
{
	close_pubnub_session();
	pubnub_session = 0;
	set_cloud_state(CLOUD_STATE_IDLE);
}

//*****************************************************************************
void set_cloud_state(enum cloud_state_enums new_state)
{
	update_bt_advert_cloud_state_flag(new_state);
	cloud_state = new_state;
}

//*****************************************************************************
// This is called whenever the device is online.  It is save to assume we have
// an open connection to the net
// Note: This function includes blocking HTTP requests.  This means we need to
// make sure that only one HTTP request happens per call as the watchdog only
// has enough time for a single HTTP timeout (5.6s wdt vs. 4.0s http timeout)
void check_cloud_state() {
	// Scan for SSIDs while we're not connected

	// Don't do anything until we've at least synced the system time
	if (!received_api_time())
		return;

	switch (cloud_state) {
	case CLOUD_STATE_IDLE:
		check_cloud_provisioning();
		break;

	case CLOUD_STATE_UNPROVISIONED:
		// Mark that this is the first time the device is connecting to the cloud
		first_connection = true;
		set_cloud_state(CLOUD_STATE_MISSING_SIGNING_KEY);
		break;

	case CLOUD_STATE_MISSING_SIGNING_KEY:
		// It's assumed the functions below will error check for unassigned parameters
		set_api_signing_secret();
		set_cloud_state(CLOUD_STATE_MISSING_PUBNUB_PARAMS);
		break;

	case CLOUD_STATE_MISSING_PUBNUB_PARAMS:
		// It's assumed the functions below will error check for unassigned parameters
		set_pubnub_params();
		set_cloud_state(CLOUD_STATE_IDLE);
		break;

	case CLOUD_STATE_WAITING_FOR_API_SYNC:
		if (api_sync_complete())
			set_cloud_state(CLOUD_STATE_PUBNUB_CLOSED);
		break;

	case CLOUD_STATE_PUBNUB_CLOSED:
		// Connect to the HTTP Server
		open_pubnub_http_session();		// This will push the states as needed
		break;

	case CLOUD_STATE_PUBNUB_IDLE:
		issue_pubnub_get_request();		// This will push the states as needed
		break;

	case CLOUD_STATE_PUBNUB_OPEN:
		process_pubnub_response();// This will push the states as needed.  Function is blocking!
		break;

	default:
		set_cloud_state(CLOUD_STATE_IDLE);
		break;
	}
}

//*****************************************************************************
char * cloud_status_msg() {
	switch (cloud_state) {
	case CLOUD_STATE_IDLE:
		return "Cloud Idle";
	case CLOUD_STATE_UNPROVISIONED:
		return "Cloud Unprovisioned";
	case CLOUD_STATE_MISSING_SIGNING_KEY:
		return "Cloud Missing Signing Key";
	case CLOUD_STATE_MISSING_PUBNUB_PARAMS:
		return "Cloud Missing Pubnub Params";
	case CLOUD_STATE_WAITING_FOR_API_SYNC:
		return "Waiting for API Sync";
	case CLOUD_STATE_PUBNUB_CLOSED:
		return "Cloud Connection Closed";
	case CLOUD_STATE_PUBNUB_IDLE:
		return "Cloud Connection Idle";
	case CLOUD_STATE_PUBNUB_OPEN:
		return "Cloud Connection Open";
	default:
		return "Cloud ERROR cloud_state!";
	}
}

/*****************************************************************************/
void check_cloud_provisioning()
{
	// If any of the connection strings are not set, assume the device is unprovisioned
	if (cloud_is_provisioned())
	{
		if (pubnub_auth_key_invalidated())
		{
			write_message("Invalid PubNub Auth Key detected");
			set_cloud_state(CLOUD_STATE_MISSING_PUBNUB_PARAMS);
		}
		else
		{
			write_message("Cloud Provisioned");
			write_message("Auth Key: %s", provisioned_params.pubnub_auth_key);
			write_message("Channel: %s", provisioned_params.pubnub_channel);
			write_message("Sub Key: %s", provisioned_params.pubnub_subscribe_key);

			update_bt_advert_provisioning_state_flag(PROVISIONING_STATE_FLAG_PROVISIONED);
			set_cloud_state(CLOUD_STATE_WAITING_FOR_API_SYNC);
		}
	}
	else
	{
		// See if we still have time to connect
		if ((provisioning_start_time >= 0) && (wmtime_time_get_posix() >= (provisioning_start_time + MAX_PROVISIONING_TIME_S)))
		{
			write_message("Timeout - Aborting provisioning, %i %i", provisioning_start_time, wmtime_time_get_posix());
			deprovision_from_cloud();
			update_bt_advert_provisioning_state_flag(PROVISIONING_STATE_FLAG_UNPROVISIONED);
			provisioning_start_time = -1;
			return;
		}

		set_cloud_state(CLOUD_STATE_UNPROVISIONED);
	}
}

/*****************************************************************************/
void cloud_handle_new_system_time(int time_change_s)
{
	if (provisioning_start_time >= 0)
	{
		write_message("Registering new time change: %i", time_change_s);
		provisioning_start_time += time_change_s;
	}
}

/*****************************************************************************/
void write_cloud_nvm_params()
{
	// Save it in NVM for future connections
	write_nvm_encrypted_value(NVM_NAME_CLOUD_PARAMS, (void*)&provisioned_params,
			sizeof(cloud_provisioned_params_struct),
			(void*)provisioned_params.nvm_magic_key);
}

/*****************************************************************************/
void register_new_cloud_params(char * new_api_id, char * new_api_connection_token)
{
	write_message("Registering new cloud params: %s - %s", new_api_id, new_api_connection_token);
	deprovision_from_cloud();
	update_bt_advert_provisioning_state_flag(PROVISIONING_STATE_FLAG_PENDING);

	strncpy((void*)provisioned_params.api_id, new_api_id, MAX_API_ID_STR_SIZE);
	strncpy((void*)api_connection_token, new_api_connection_token,	MAX_API_CONNECTION_TOKEN_STR_SIZE);

	// Save the credentials
	write_cloud_nvm_params();

	provisioning_start_time = wmtime_time_get_posix();
}

/*****************************************************************************/
// Note: msg is expected to be null terminated
int handle_pubnub_framing(char * msg) {
	char * msg_start_pos = msg;
	char * msg_end_pos;

	while (*msg_start_pos != 0)
	{
		switch (msg_start_pos[0])
		{
			// Receiving a JSON message
			case '{':
				msg_end_pos = strchr(msg_start_pos + 1, '}');
				if (msg_end_pos == NULL)
					goto func_exit;
				handle_pubnub_message(msg_start_pos);
				msg_start_pos = msg_end_pos + 1;
				break;

				// Receiving a timestamp
			case '\"':
				msg_end_pos = strchr(msg_start_pos + 1, '\"');
				if (msg_end_pos == NULL)
					goto func_exit;
				*msg_end_pos = 0;
				msg_start_pos++;
				register_new_pubnub_timemark(atoll(msg_start_pos), true);
				msg_start_pos = msg_end_pos + 1;
				break;

			default:
				msg_start_pos++;
				break;
		}
	}

func_exit:
	return msg_start_pos - msg;
}

/*****************************************************************************/
// Note: msg is expected to be null terminated
void handle_pubnub_message(char * msg) {
	// Initialize the JSON parser and parse the given string
	if (json_init(&json_parser, json_tokens, JSON_PARSER_MAX_TOKENS, msg,
			strlen(msg)) != WM_SUCCESS)
		return;

	// Get the value of "now" which is a string of the time value
	light_state_struct new_light_state = light_state;
	bool new_lighting_values_received = false;

	new_light_state.pulse_speed = 0;
	new_light_state.pulse_lighting = false;

	bool api_update_requested = false;

	if (json_get_val_bool(&json_parser, "on",
			(bool*) &new_light_state.power_state) == WM_SUCCESS)
		new_lighting_values_received = true;
	if (json_get_val_int(&json_parser, "dim",
			(int*) &new_light_state.target_level) == WM_SUCCESS)
		new_lighting_values_received = true;
	if (json_get_val_bool(&json_parser, "update", &api_update_requested) == WM_SUCCESS)
		if (api_update_requested)
			force_api_update();

	if (new_lighting_values_received)
	{
		#if (CORDELIA_BOARD)
			configure_cordelia(new_light_state.power_state, new_light_state.target_level);
		#else
			set_lighting(new_light_state);
		#endif
	}
}

/*****************************************************************************/
void close_pubnub_session() {
	if (pubnub_session != 0)
		http_close_session(&pubnub_session);

	if (cloud_state > CLOUD_STATE_PUBNUB_CLOSED)
		set_cloud_state(CLOUD_STATE_PUBNUB_CLOSED);
}

/*****************************************************************************/
void open_pubnub_http_session() {
	close_pubnub_session();

	strncpy(wifi_temp_buffer, "https://pubsub.pubnub.com", WIFI_TEMP_BUFFER_SIZE);

	// Initiate a connection with the remote Web Server using a call to http_open_session().
	//if (http_open_session(&pubnub_session, wifi_temp_buffer, 0, NULL, 0) == WM_SUCCESS)
	if (http_open_session_with_rcv_timeout(&pubnub_session, wifi_temp_buffer, NULL, HTTP_GET_TIMEOUT_MS) == WM_SUCCESS)
		set_cloud_state(CLOUD_STATE_PUBNUB_IDLE);
	else
		write_message("Pubnub Connect Failed");
}

/*****************************************************************************/
void issue_pubnub_get_request() {
	snprintf(wifi_temp_buffer, WIFI_TEMP_BUFFER_SIZE,
			"pubsub.pubnub.com/subscribe/%s/%s/0/%lld>?auth=%s&uuid=%s",
			provisioned_params.pubnub_subscribe_key,
			provisioned_params.pubnub_channel, load_active_pubnub_timemark(),
			provisioned_params.pubnub_auth_key,
			get_api_id());

	write_message("API ID: %s", get_api_id());

	// Setup the parameters for the HTTP Request.
	http_req_t http_req;
	set_http_req_params(pubnub_session, &http_req, HTTP_GET, wifi_temp_buffer, NULL, STANDARD_HDR_FLAGS | HDR_ADD_CONN_KEEP_ALIVE);
	http_add_header(pubnub_session, &http_req, "timeout", "5");

	// Send the HTTP GET request, that was prepared earlier, to the web server
	if (http_send_request(pubnub_session, &http_req) != WM_SUCCESS) {
		close_pubnub_session();
		return;
	}

	set_cloud_state(CLOUD_STATE_PUBNUB_OPEN);
	pubnub_session_opened_timemark = wmtime_time_get_posix();
}

/*****************************************************************************/
// This will block!
void process_pubnub_response()
{
	// Start by adjusting lighting indicating we're all the way to waiting for pubnub data
	if (first_connection)
	{
		light_state_struct light_vals = { .power_state = LIGHTS_ON,	.target_level = 2, .pulse_lighting = 1, .pulse_speed = 2 };
		set_lighting(light_vals);

		first_connection = false;
	}

	// Read the response and make sure that an HTTP_OK response is received
	// A failure here likely means the pubnub session timed out and that no new commands had been received.
	http_resp_t *resp;
	if (http_get_response_hdr(pubnub_session, &resp) != WM_SUCCESS) {
		if ((errno != EAGAIN) ||
				((wmtime_time_get_posix() - pubnub_session_opened_timemark) > PUBNUB_SESSION_DURATION_S))
		{
			write_message("Pubnub Receive Timeout. Closing Connection: %i", wmtime_time_get_posix());
			close_pubnub_session();
		}

		return;
	}

	// Check that we've gotten a proper response
	if (resp->status_code != HTTP_OK)
	{
		if (resp->status_code == HTTP_FORBIDDEN)
		{
			// Our Pubnub Aut Token has expired or is otherwise invalid.  Let's get a new one
			invalidate_pubnub_auth_key();
			write_message("PubNub Auth Token Invalid. Refreshing.");
		}
		else
			write_message("PubNub Request Invalid. Code: %i", resp->status_code);

		// Do not continue without a valid session
		reset_cloud_state_machine();
		return;
	}

	int saved_byte_count = 0;
	int received_byte_count;
	do
	{
		memset(wifi_temp_buffer + saved_byte_count, 0, WIFI_TEMP_BUFFER_SIZE - saved_byte_count);	// Ensures null termination
		received_byte_count = http_read_content(pubnub_session, wifi_temp_buffer + saved_byte_count, WIFI_TEMP_BUFFER_SIZE - 1 - saved_byte_count);
		if (received_byte_count)
		{
			write_message("Pubnub Msg: %s", wifi_temp_buffer);
			int bytes_handled = handle_pubnub_framing(wifi_temp_buffer);
			if (bytes_handled == 0)
			{
				write_message("Error processing PubNub data (message likely too long).");
				write_message("Dropping queued message(s)!");
				reset_pubnub_timemark();
				break;
			}
			else
			{
				memmove(wifi_temp_buffer, wifi_temp_buffer + bytes_handled, WIFI_TEMP_BUFFER_SIZE - bytes_handled);
				saved_byte_count += received_byte_count - bytes_handled;
			}
		}
	} while (received_byte_count > 0);

	set_cloud_state(CLOUD_STATE_PUBNUB_IDLE);
}

/*****************************************************************************/
//
void set_api_signing_secret() {
	if ((strlen(get_api_id()) == 0) || (strlen(api_connection_token) == 0))
		return;

	// Update the sys time
	write_message("Requesting Signing Key");

	snprintf(wifi_temp_buffer, WIFI_TEMP_BUFFER_SIZE, API_DOMAIN "/device/%s/signingKey", get_api_id());
	http_req_t http_req;
	http_session_t http_session = 0;

	// Initiate a connection with the remote Web Server using a call to http_open_session().
	// int status = http_open_session(&http_session, wifi_temp_buffer, 0, NULL, 0);
	int status = http_open_session(&http_session, wifi_temp_buffer, NULL);
	if (status != WM_SUCCESS)
		goto function_exit;

	// Create the content of our message
	char temp_buffer[128];
	snprintf(temp_buffer, 128, "{\"token\":\"%s\"}", api_connection_token);

	// Setup the parameters for the HTTP Request. This only prepares the request to be sent out
	set_http_req_params(http_session, &http_req, HTTP_POST, wifi_temp_buffer,
			temp_buffer, STANDARD_HDR_FLAGS);

	if (send_request(http_session, &http_req, wifi_temp_buffer,
			WIFI_TEMP_BUFFER_SIZE) != WM_SUCCESS)
		goto function_exit;

	// Initialize the JSON parser and parse the given string
	int error = json_init(&json_parser, json_tokens, JSON_PARSER_MAX_TOKENS,
			wifi_temp_buffer, strlen(wifi_temp_buffer));
	if (error != WM_SUCCESS)
		goto function_exit;

	// Get the value of "key" which is a string of the time value
	if (json_get_val_str(&json_parser, "key", (void*)provisioned_params.api_signing_key, MAX_API_SIGNING_KEY_STR_SIZE + 1) != WM_SUCCESS)
		provisioned_params.api_signing_key[0] = 0;

	// Save the new parameters
	write_cloud_nvm_params();

	function_exit: if (http_session != 0)
		http_close_session(&http_session);
}

/*****************************************************************************/
void set_pubnub_params() {
	if ((strlen(get_api_id()) == 0) || (strlen(get_singing_key()) == 0))
		return;

	int request_time = wmtime_time_get_posix();

	// Update the sys time
	write_message("Requesting PubNub Params");

	snprintf(wifi_temp_buffer, WIFI_TEMP_BUFFER_SIZE,
			API_DOMAIN "/device/%s/authKey?request_time=%i",
			get_api_id(), request_time);

	http_req_t http_req;
	http_session_t http_session = 0;

	// Initiate a connection with the remote Web Server using a call to http_open_session().
	//int status = http_open_session(&http_session, wifi_temp_buffer, 0, NULL, 0);
	int status = http_open_session(&http_session, wifi_temp_buffer, NULL);
	if (status != WM_SUCCESS)
		goto function_exit;
	// Setup the parameters for the HTTP Request. This only prepares the request to be sent out
	set_http_req_params(http_session, &http_req, HTTP_GET, wifi_temp_buffer,
			NULL, STANDARD_HDR_FLAGS);
	// Create the Authentication Message
	snprintf(wifi_temp_buffer, WIFI_TEMP_BUFFER_SIZE,
			"%i/device/%s/authKey?request_time=%i", request_time, get_api_id(),
			request_time);

	set_authorization_header(http_session, &http_req, wifi_temp_buffer);
	if (send_request(http_session, &http_req, wifi_temp_buffer,	WIFI_TEMP_BUFFER_SIZE) != WM_SUCCESS)
		goto function_exit;

	// Initialize the JSON parser and parse the given string
	if (json_init(&json_parser, json_tokens, JSON_PARSER_MAX_TOKENS, wifi_temp_buffer, strlen(wifi_temp_buffer)) != WM_SUCCESS)
		goto function_exit;

	// Get the value of "key" which is a string of the time value
	if (json_get_val_str(&json_parser, "auth_key",
			(void*)provisioned_params.pubnub_auth_key,
			MAX_PUBNUB_AUTH_KEY_STR_SIZE + 1) != WM_SUCCESS)
		provisioned_params.pubnub_auth_key[0] = 0;
	if (json_get_val_str(&json_parser, "channel",
			(void*)provisioned_params.pubnub_channel,
			MAX_PUBNUB_CHANNEL_STR_SIZE + 1) != WM_SUCCESS)
		provisioned_params.pubnub_channel[0] = 0;
	if (json_get_val_str(&json_parser, "sub_key",
			(void*)provisioned_params.pubnub_subscribe_key,
			MAX_PUBNUB_SUBSCRIBE_KEY_STR_SIZE + 1) != WM_SUCCESS)
		provisioned_params.pubnub_subscribe_key[0] = 0;

	// Save the new parameters
	write_cloud_nvm_params();

	function_exit: if (http_session != 0)
		http_close_session(&http_session);
}

/*****************************************************************************/
char * get_api_id() {
	return (char*)provisioned_params.api_id;
}

/*****************************************************************************/
char * get_singing_key() {
	return (char*)provisioned_params.api_signing_key;
}

/*****************************************************************************/
void invalidate_pubnub_auth_key()
{
	strcpy((void*)provisioned_params.pubnub_auth_key, "INVALID");
	write_cloud_nvm_params();
}

/*****************************************************************************/
bool pubnub_auth_key_invalidated()
{
	return (strcmp((void*)provisioned_params.pubnub_auth_key, "INVALID") == 0);
}


/*****************************************************************************/
void deprovision_from_cloud()
{
	reset_cloud_state_machine();

	// Invalid all the parameters
	api_connection_token[0] = 0;
	provisioned_params.api_id[0] = 0;
	provisioned_params.pubnub_channel[0] = 0;
	provisioned_params.pubnub_subscribe_key[0] = 0;
	provisioned_params.pubnub_auth_key[0] = 0;
	provisioned_params.api_signing_key[0] = 0;
}

/*****************************************************************************/
bool cloud_is_provisioned()
{
	return ((strlen((void*)provisioned_params.pubnub_channel) > 0)
			&& (strlen((void*)provisioned_params.pubnub_subscribe_key) > 0)
			&& (strlen((void*)provisioned_params.pubnub_auth_key) > 0)
			&& (strlen((void*)provisioned_params.api_id) > 0)
			&& (strlen((void*)provisioned_params.api_signing_key) > 0));
}
