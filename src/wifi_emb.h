#ifndef _WIFI_EMB_H_
#define _WIFI_EMB_H_

	#include <wlan.h>	// Needed for SSID MAX SIZEs
	#include <wm_os.h> // Needed for Mutex

	#define	WIFI_TEMP_BUFFER_SIZE		(196)
	#define MAX_SSID_NAME_SIZE			(WLAN_NETWORK_NAME_MAX_LENGTH)
	#define MAX_SSID_PASSPHRASE_SIZE	(WLAN_PSK_MAX_LENGTH)
	#define SSID_LIST_BUFFER_SIZE		(512)	// Defined by the max size of a long BLE read

	enum wifi_state_flags_enums
	{
		WIFI_STATE_FLAG_UNINITIALIZED	= 0x00,
		WIFI_STATE_FLAG_INIT	 		= 0x01,
		WIFI_STATE_FLAG_SCANNING		= 0x02,
		WIFI_STATE_FLAG_IDLE			= 0x03,
		WIFI_STATE_FLAG_CONNECTING		= 0x04,
		WIFI_STATE_FLAG_THREAD_RUNNING	= 0x05,
		WIFI_STATE_FLAG_WAITING_FOR_RF	= 0x06,
		WIFI_STATE_FLAG_HTTP_TEST		= 0x07,
		WIFI_STATE_FLAG_CONNECTED		= 0x10,
		WIFI_STATE_FLAG_UNKNOWN_STATE	= 0xFF,
	};

	enum wifi_error_flags_enums
	{
		WIFI_ERROR_FLAG_CLEAR_ERRORS		= 0,
		WIFI_ERROR_FLAG_NETWORK_DISCONNECT	= (1 << 0),
		WIFI_ERROR_FLAG_ADDRESS_FAILED		= (1 << 1),
		WIFI_ERROR_FLAG_AUTH_FAILED			= (1 << 2),
		WIFI_ERROR_FLAG_UNKNOWN_CALLBACK	= (1 << 3),
		WIFI_ERROR_FLAG_NETWORK_NOT_FOUND	= (1 << 4),
		WIFI_ERROR_FLAG_SSID_LIST_LOCKED	= (1 << 5),
		WIFI_ERROR_FLAG_CONNECT_FAILED		= (1 << 6),
	};

	void initialize_wifi();
	void set_new_wifi_ssid(char * name);
	void set_new_wifi_passphrase(char * ssid);
	void wifi_connect_to_new_network();
	char * wifi_status_msg();
	void report_scanned_ssids();
	void control_wifi_test_mode(bool enable_test_mode, char * test_mode_url);
	bool wifi_is_connected();
	int copy_ssid_list(char * dest_ptr);
	void get_wifi_strenth(short * rssi, int * snr);

	extern char wifi_temp_buffer[WIFI_TEMP_BUFFER_SIZE];

#endif
