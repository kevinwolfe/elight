// Note, the following are defined by the wicentric make file (...\stack\build\common.mak)
// and also in settings.h (as included by wmcrypto.h).  By undefining them here, we suppress
// a GCC warning.  I suspect Marvell will fix this in the future, but until then...
#undef WMSDK_BUILD
#undef NO_DEV_RANDOM
#undef NO_PSK
#undef NO_FILESYSTEM
#undef FREERTOS
#undef CYASSL_LWIP

#include "update_fw.h"
#include <secure_upgrade.h>
#include <chacha20.h>
#include <ed25519.h>
#include <hkdf-sha.h>

#include "fw_keys_public.h"
#include "uart.h"

#define CHACHA_NONCE_LEN		8
#define DECRYPT_KEY_LEN			32
#define VERIFICATION_KEY_LEN	32

static void update_firmware_thread();


chacha20_ctx_t chctx;
SHA512Context     sha;
uint8_t hash[64];
ed25519_signature ed_sign;
uint8_t nonce[CHACHA_NONCE_LEN];

static bool run_fw_update_flag = false;		// Used to communicate between threads
static char fw_update_url[FW_UPDATE_PATH_MAX_LEN];

//*****************************************************************************
// Initialize the Chacha20 context.
void decrypt_init_func(void *context, void *key, void *iv)
{
	chacha20_init(context, key, DECRYPT_KEY_LEN, iv, CHACHA_NONCE_LEN);
}

//*****************************************************************************
// This function needs to be defined to match the signature of decrypt_func_t
// We will use chacha20 for decryption.
int decrypt_func(void *context, void *inbuf, int inlen, void *outbuf, int outlen)
{
	if (outlen < inlen)
		return -WM_FAIL;
	chacha20_decrypt(context, inbuf, outbuf, inlen);
	return inlen;
}

//*****************************************************************************
// This function needs to be defined to match the signature of
// sign_verify_func_t. We will use ED25519 for verification.
int signature_verify_func(void *message, int len, void *pk, void *signature)
{ 
	int return_val = ed25519_sign_open(message, len, pk, signature);
	return return_val;
}

//*****************************************************************************
// This function needs to be defined to match the signature of
// hash_update_func_t. We will use SHA512
void hash_update_func(void *context, void *buf, int len)
{
	SHA512Input(context, buf, len);
}

//*****************************************************************************
// This function needs to be defined to match the signature of
// hash_finish_func_t. We will use SHA512.
void hash_finish_func(void *context, void *result)
{
	SHA512Result(context, result);
}

upgrade_struct_t priv_data = {
	.decrypt_init = decrypt_init_func,
	.decrypt = decrypt_func,
	.signature_verify = signature_verify_func,
	.hash_update = hash_update_func,
	.hash_finish = hash_finish_func,
	.decrypt_ctx = &chctx,
	.fw_data_read_fn = 0,
	.hash_ctx = &sha,
	.iv = &nonce,
	.iv_len = 8,
	.signature = &ed_sign,
	.signature_len = 64,
	.hash = &hash,
	.hash_len = 64,
};


//*****************************************************************************
void run_fw_update(char * fw_url)
{
	strncpy((char *)fw_update_url, (const char *) fw_url, FW_UPDATE_PATH_MAX_LEN - 1);
	run_fw_update_flag = true;
}


//*****************************************************************************
void initialize_fw_updater()
{
	// The updater puts a lot of stuff on the stack.  Create it's own thread for it.
	xTaskCreate(update_firmware_thread, (signed char *)"update_firmware_thread", 1200, NULL, 1, NULL);
	
	// Check the stack level with the command below:
	 //write_message("Thread's Stack space remaining: %i", uxTaskGetStackHighWaterMark( NULL ));
}

//*****************************************************************************
void update_firmware_thread()
{
	while(1)
	{
		if (run_fw_update_flag)
		{
			write_message("Thread's Stack space remaining: %i", uxTaskGetStackHighWaterMark( NULL ));
			run_fw_update_flag = false;

			write_message("Upgrading firmware from: %s", fw_update_url);
			SHA512Reset(priv_data.hash_ctx);
			priv_data.pk = verification_public_key;
			priv_data.decrypt_key = decrypt_key;

			//if (secure_upgrade(fw_update_url, &priv_data, NULL) != WM_SUCCESS)
			if (client_mode_secure_upgrade(FC_COMP_FW, fw_update_url, &priv_data, NULL)  != WM_SUCCESS)
			{
				write_message("Firmware upgrade failed!");
				continue;
			}

			write_message("Firmware upgrade successful!");
			write_message("Soft Rebooting...");
			pm_reboot_soc();
		}
		else
			os_thread_sleep(os_msec_to_ticks(100));
	}
}
