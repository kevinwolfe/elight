#ifndef _GAP_SERVICE_H_
#define _GAP_SERVICE_H_

#include "main_gap_declares.h"
#include "main_gap_api.h"

typedef uint8 tgap_proc_req_code;

// GAPS Request Opcodes
#define GAP_READ_REMOTE_NAME_REQ            0x01
#define GAP_START_SERVICE_SEARCH_REQ        0x02

typedef union
{
	tBT_UUID service_uuid;
} tgaps_req_api_params;

void gap_serv_init(void);
extern bt_result Gaps_Client_Request(tgap_proc_req_code req_code, tbt_device_id *device_id,tgaps_req_api_params *params, bt_gap_proc_status_callback req_cb);

#endif //_GAP_SERVICE_H_
