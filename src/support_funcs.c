#include "support_funcs.h"

#include "base64.h"
#include "uart.h"
#include "wmcrypto.h"
#include "cloud.h"

#include <mdev_aes.h>

static unsigned char const aes_key[] = {0xc2, 0x86, 0x69, 0x6d, 0x88, 0x7c, 0x9a, 0xa0, 0x61, 0x1b, 0xbb, 0x3e, 0x20, 0x25, 0xa4, 0x5a};

/*****************************************************************************/
void set_http_req_params(http_session_t http_session, http_req_t * http_req, http_method_t http_method, char * http_resource, char * req_content, http_hdr_field_sel_t flags)
{
	http_req->type = http_method;
	http_req->resource = http_resource;
	http_req->version = HTTP_VER_1_1;
	http_req->content = req_content;
	http_req->content_len = strlen(http_req->content);

	http_prepare_req(http_session, http_req, flags);
	http_add_header(http_session, http_req, "Accept", "application/json");
	http_add_header(http_session, http_req, "Content-Type", "application/json");
}

/*****************************************************************************/
void set_authorization_header(http_session_t http_session, http_req_t * http_req, char * encoding_message)
{
	uint8_t hash[SHA256_HASH_SIZE];			// 32 * 8 = 256 Bits
	char hash_base64[SHA256_BASE64_HASH_SIZE + 1];

	mrvl_hmac_sha256((const uint8_t *)get_singing_key(), strlen((const char *)get_singing_key()), \
					 (uint8_t *)encoding_message, strlen((const char *)encoding_message), \
					 hash, SHA256_HASH_SIZE);

	Base64encode(hash_base64, (char*)hash, SHA256_HASH_SIZE);
	sprintf(encoding_message, "EMBHM256 %s.%s", get_api_id(), hash_base64);
	http_add_header(http_session, http_req, "Authorization", encoding_message);
}

/*****************************************************************************/
int send_request(http_session_t http_session, http_req_t * http_req, char * response_buffer, int max_response_data_size)
{
	// Send the HTTP GET request, that was prepared earlier, to the web server
	if (http_send_request(http_session, http_req) != WM_SUCCESS)
		return WM_FAIL;

	// Read the response and make sure that an HTTP_OK response is received
	http_resp_t *resp;
	if ((http_get_response_hdr(http_session, &resp) != WM_SUCCESS) || (resp->status_code != HTTP_OK))
	{
		write_message("HTTP Error %i, %s", resp->status_code, resp->reason_phrase);
		return resp->status_code;
	}

	memset(response_buffer, 0, max_response_data_size);
	http_read_content(http_session, response_buffer, max_response_data_size - 1);

	write_message("HTTP Response: %s", response_buffer);

	return WM_SUCCESS;
}

/*****************************************************************************/
static bool aes_initialized = false;
static aes_t enc_aes;
static mdev_t *aes_dev;

void initialize_aes()
{
	if (!aes_initialized)
	{
		aes_drv_init();
		aes_dev = aes_drv_open(MDEV_AES_0);
		aes_initialized = true;
	}
}

/*****************************************************************************/
void encrypt_buffer(const uint8_t * plain_text, uint8_t * cipher_text, uint8_t * iv, int len)
{
	initialize_aes();
	aes_drv_setkey(aes_dev, &enc_aes, aes_key, AES_BLOCK_SIZE, iv, AES_ENCRYPTION, HW_AES_MODE_CTR);
	aes_drv_encrypt(aes_dev, &enc_aes, plain_text, cipher_text, len);
}

/*****************************************************************************/
void decrypt_buffer(const uint8_t * cipher_text, uint8_t * plain_text, uint8_t * iv, int len)
{
	initialize_aes();
	aes_drv_setkey(aes_dev, &enc_aes, aes_key, AES_BLOCK_SIZE, iv, AES_DECRYPTION, HW_AES_MODE_CTR);
	aes_drv_decrypt(aes_dev, &enc_aes, cipher_text, plain_text, len);
}

