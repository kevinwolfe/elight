#include "io.h"

#include <mdev_adc.h>
#include <mdev_gpio.h>
#include <wmstdio.h>
#include <mc200/mc200_gpio.h>
#include <mdev_pinmux.h>
#include "mdev_i2c.h"
#include <wmerrno.h>
#include "light_control.h"
#include "main.h"
#include "uart.h"
#include "elight_board.h"
#include "timers_emb.h"
#include <wifi.h>
#include <wmtime.h>
#include "schedules.h"

#include <wm_os.h>			// Needed for os_thread_sleep()

#if (MC200_DEV_BOARD)
	#define GPIO_ZERO_CROSSING		(GPIO_1)
	#define GPIO_TRIAC_CONTROL		(GPIO_3)
#else
	#define GPIO_ZERO_CROSSING		(GPIO_5)
	#define GPIO_TRIAC_CONTROL		(GPIO_6)
#endif

#define GPIO_LIGHTING_ENABLE	(GPIO_9)
#define GPIO_MFG_LED_GREEN		(GPIO_8)	// ADC1
#define GPIO_MFG_LED_RED		(GPIO_7)	// ADC0
#define GPIO_MOTION_SENSOR		(GPIO_7)	// ADC0

#define ADC_0					(GPIO_7)
#define ADC_1					(GPIO_8)
#define LED_RELAY_CONTROL		(GPIO_5)

#if (CORDELIA_BOARD)
	void motion_sensor_trip_cb();

	static int sensor_trip_duration_sec;
	static volatile bool sensor_enabled;
#endif

#define TRC_LIGHT_ON    			(1)
#define TRC_LIGHT_OFF   			(0)

static mdev_t * gpio_dev;
static mdev_t * i2c_dev;
static mdev_t * adc_dev;

void zero_crossing_cb();
void short_alarm_cb();
volatile int	zero_crossing_count				= 0;
volatile bool	zero_cross_gpio_level			= 0;
volatile bool	lighting_enable_gpio_level		= 0;
volatile bool	lighting_control_enabled		= false;

static volatile bool 	mfg_led_blink_red		= false;
static volatile bool 	mfg_led_blink_green		= false;
static volatile bool 	mfg_drive_20hz			= false;

static volatile bool 	alarm_handled			= false;

void set_gpio_value(int gpio, int val);
int get_gpio_value(int gpio);
void io_test_thread();
void set_mfg_led(int gpio, char new_led_state);

void * io_thread_handle;

//****************************************************************************
void set_led_relay(int state)
{
	set_gpio_value(LED_RELAY_CONTROL, state);
}


//****************************************************************************
void set_gpio_value(int gpio, int val)
{
	gpio_drv_write(gpio_dev, gpio, val);
}

//****************************************************************************
// WARNING! Using this function in conjuction with GPIO ISRs has been known
// to cause random resets when called from multiple threads
int get_gpio_value(int gpio)
{
	int value = -1;
	gpio_drv_read(gpio_dev, gpio, &value);
	return value;
}

//****************************************************************************
void set_tcr(bool state)
{
	if (lighting_control_enabled && !mfg_drive_20hz)
		set_gpio_value(GPIO_TRIAC_CONTROL, state);
}

//****************************************************************************
void initialize_io()
{
    // Initialise IO Pins
	gpio_drv_init();										// Initialize GPIO driver
	gpio_dev = gpio_drv_open("MDEV_GPIO");					// Open GPIO driver

	#if (LED_DRIVER)
		pinmux_drv_init();											// Initialize  pinmux driver
		mdev_t * pinmux_dev = pinmux_drv_open("MDEV_PINMUX");		// Open pinmux driver
		pinmux_drv_setfunc(pinmux_dev, GPIO_TRIAC_CONTROL, GPIO6_GPT3_CH0);
		gpio_drv_setdir(gpio_dev, GPIO_ZERO_CROSSING,	GPIO_OUTPUT);	// Configure GPIO pin direction as Output
		gpio_drv_setdir(gpio_dev, LED_RELAY_CONTROL,	GPIO_OUTPUT);	// Configure GPIO pin direction as Output
	#else
		gpio_drv_setdir(gpio_dev, GPIO_TRIAC_CONTROL,	GPIO_OUTPUT);	// Configure GPIO pin direction as Output
		set_gpio_value(GPIO_TRIAC_CONTROL, false);
		gpio_drv_setdir(gpio_dev, GPIO_ZERO_CROSSING,	GPIO_INPUT);	// Configure GPIO pin direction as Input

		// Register a callbacks for the GPIOs
		gpio_drv_set_cb(gpio_dev, GPIO_ZERO_CROSSING,	GPIO_INT_BOTH_EDGES, 	NULL, 	zero_crossing_cb);

	#endif


	gpio_drv_setdir(gpio_dev, GPIO_LIGHTING_ENABLE,	GPIO_INPUT);	// Configure GPIO pin direction as Input

    // If the value is low at boot, don't ever allow it
    lighting_control_enabled = get_gpio_value(GPIO_LIGHTING_ENABLE);

	// EVT2 units will have an external pull-up, but EVT1 & breadboard units do not (GPIO pins are left floating)
	// To maintain backwards compatibility, enable the internal pull-up on the GPIO pin
	GPIO_PinModeConfig(GPIO_LIGHTING_ENABLE, PINMODE_PULLUP);

	// Initialize I2C Driver
	i2c_drv_init(board_mfi_i2c_port_id());
	i2c_drv_timeout(board_mfi_i2c_port_id(), 1000, 1000);

	// i2c_dev is configured as master
	i2c_dev = i2c_drv_open(board_mfi_i2c_port_id(), (0x22 >> 1) | I2C_CLK_400KHZ);

	#if (LED_DRIVER)
		adc_drv_init(ADC0_ID);

		// Get default ADC gain value
		ADC_CFG_Type 	adc_config;
		adc_get_config(&adc_config);
		write_message("Default ADC gain value = %d\r\n", adc_config.adcGainSel);

		// Modify ADC gain to 2
		adc_modify_default_config(adcGainSel, ADC_GAIN_2);

		adc_get_config(&adc_config);
		write_message("Modified ADC gain value to %d\r\n", adc_config.adcGainSel);

		adc_dev = adc_drv_open(ADC0_ID, ADC_CH0);
	#else
		// Setup ADC0 to measure onboard temp sensor
		adc_drv_init(ADC0_ID);
		adc_dev = adc_drv_open(ADC0_ID, ADC_TEMPP);
		ADC_ModeSelect(ADC0_ID, ADC_MODE_TSENSOR);
		adc_drv_calib(adc_dev, 0);

		#if (CORDELIA_BOARD)
			gpio_drv_setdir(gpio_dev, GPIO_MFG_LED_GREEN,	GPIO_INPUT);	// Configure GPIO pin direction as Input
			gpio_drv_setdir(gpio_dev, GPIO_MOTION_SENSOR,	GPIO_INPUT);	// Configure GPIO pin direction as Input
			gpio_drv_set_cb(gpio_dev, GPIO_MOTION_SENSOR,	GPIO_INT_FALLING_EDGE, 	NULL,	motion_sensor_trip_cb);

			sensor_trip_duration_sec	= 15;
			sensor_enabled				= true;
		#else
			#if (PRODUCTION_BUILD)
	//			gpio_drv_set_cb(gpio_dev, GPIO_LIGHTING_ENABLE,	GPIO_INT_BOTH_EDGES, 	NULL,	short_alarm_cb);
				gpio_drv_setdir(gpio_dev, GPIO_MFG_LED_RED,		GPIO_INPUT);	// Configure GPIO pin direction as Input
				gpio_drv_setdir(gpio_dev, GPIO_MFG_LED_GREEN,	GPIO_INPUT);	// Configure GPIO pin direction as Input
			#else
				gpio_drv_setdir(gpio_dev, GPIO_MFG_LED_RED,		GPIO_OUTPUT);	// Configure GPIO pin direction as Output
				gpio_drv_setdir(gpio_dev, GPIO_MFG_LED_GREEN,	GPIO_OUTPUT);	// Configure GPIO pin direction as Output

				xTaskCreate(io_thread, (signed char *)"io_test_thread", 150, NULL, 1, &io_thread_handle);
			#endif
		#endif
	#endif
}

char zc_history;
char alarm_history;

#if (PRODUCTION_BUILD)

//****************************************************************************
void short_alarm_cb()
{
	if (lighting_control_enabled)
	{
		if (get_gpio_value(GPIO_LIGHTING_ENABLE) == false)
			start_alarm();
		else
			cancel_alarm();
	}
}

//****************************************************************************
void alarm_triggered()
{
	lighting_control_enabled = false;
	set_gpio_value(GPIO_TRIAC_CONTROL, TRC_LIGHT_OFF);
	write_message("*** WARNING *** Hardware Alarm Triggered! Lighting Control is disabled!");
}


#else

char zc_history;
char alarm_history;
//****************************************************************************
void io_test_thread()
{
	int		alarm_debounce_cnt	= 2;

	int		led_blink_cnt		= 0;
	bool	led_blink_state		= MFG_LED_OFF;
	bool	simulated_zc_state	= false;

	int		io_thread_ticks = 0;

	set_gpio_value(GPIO_MFG_LED_GREEN,	MFG_LED_OFF);
	set_gpio_value(GPIO_MFG_LED_RED,	MFG_LED_ON);


	// Give the UART 2 seconds to boot up before messaging the user
	os_thread_sleep(os_msec_to_ticks(2 * 1000));

	while (1)
	{
		os_thread_sleep(os_msec_to_ticks(1));

		if (++io_thread_ticks == 25)
		{
			io_thread_ticks = 0;
			zc_history = (zc_history << 1) | (get_gpio_value(GPIO_ZERO_CROSSING) > 0);
			alarm_history = (alarm_history << 1) | (get_gpio_value(GPIO_LIGHTING_ENABLE) > 0);

			if (mfg_drive_20hz)
			{
				simulated_zc_state ^= 1;
				set_gpio_value(GPIO_TRIAC_CONTROL, simulated_zc_state);
			}

			if (led_blink_cnt++ == 10)
			{
				led_blink_cnt = 0;
				led_blink_state ^= 1;

				if (mfg_led_blink_green)
					set_gpio_value(GPIO_MFG_LED_GREEN, led_blink_state);

				if (mfg_led_blink_red)
					set_gpio_value(GPIO_MFG_LED_RED, led_blink_state);
			}
		}
	}

}

//****************************************************************************
void get_loopbacks(char * zc_hist, char * alarm_hist)
{
	*zc_hist = zc_history;
	*alarm_hist = alarm_history;
}


//****************************************************************************
void drive_20hz()
{
	write_message("Enabling 20 MHz out on MB_DIM");
	mfg_drive_20hz = true;
}

//****************************************************************************
void set_mfg_green_led(char new_led_state)
{
	write_message("Setting Green MFG LED State: %i", new_led_state);
	set_mfg_led(GPIO_MFG_LED_GREEN, new_led_state);
}

//****************************************************************************
void set_mfg_red_led(char new_led_state)
{
	write_message("Setting Red MFG LED State: %i", new_led_state);
	set_mfg_led(GPIO_MFG_LED_RED, new_led_state);
}

//****************************************************************************
void set_mfg_led(int gpio, char new_led_state)
{
	bool * mfg_led_blink_ptr = (bool *)&mfg_led_blink_green;
	if (gpio == GPIO_MFG_LED_RED)
		mfg_led_blink_ptr = (bool *)&mfg_led_blink_red;

	if (new_led_state == MFG_LED_BLINK)
		*mfg_led_blink_ptr = true;
	else
	{
		*mfg_led_blink_ptr = false;
		set_gpio_value(gpio, new_led_state);
	}
}
#endif

//****************************************************************************
float read_adc()
{
	uint16_t buffer = adc_drv_result(adc_dev);

	ADC_CFG_Type 	adc_config;
	adc_get_config(&adc_config);

	#define BIT_RESOLUTION_FACTOR 32768	/* For 16 bit resolution (2^15-1) */
	#define VMAX_IN_mV	1200	/* Max input voltage in milliVolts */

	float result = ((float)buffer / BIT_RESOLUTION_FACTOR) * VMAX_IN_mV *
			((float)1/(float)(adc_config.adcGainSel != 0 ? adc_config.adcGainSel : 0.5));

	return result;
}



static bool start_firing_tcr = false;
//****************************************************************************
void zero_crossing_cb()
{
	// Elights have two different zero crossing schemes
	// One sends a pulse (~0.75 mSec wide) every time the AC signal zero crosses (e.g. two pulses per AC period)
	// The other version toggles the zero crossing signal every zero cross (e.g. a square wave w/ period equal to AC period)
	// To have the same software work with both, we interrupt on both edges and we use a 2 msec timer.  When a zero crossing is detected,
	// we start the timer and register the zero crossing. On subsequent edges, if the timer expired, we're using scheme #2 and it's safe to
	// register the zero crossing.  If the timer hasn't expired, we detected scheme #1's pulse's negative edge and it's safe to ignore
	if (two_msec_timer_expired())
	{
		// Only start driving the triac on when Zero-Crossing signal is low
		// This is so that we catch the very first alarm possible as alarm
		// is only generated on half the AC waveform
		if (start_firing_tcr == false)
			start_firing_tcr = !(get_gpio_value(GPIO_ZERO_CROSSING));

		if (start_firing_tcr)
		{
			begin_two_msec_timer();
			zero_crossing_count++;
			zcr_adjust_lighting();
		}
	}
}

//****************************************************************************
void test_mfi()
{
	int mfi_address = 0;
	for (mfi_address = 0; mfi_address < 5; mfi_address++)
		read_mfi_address(mfi_address);
}

//****************************************************************************
void read_mfi_address(uint8_t mfi_address)
{
	uint8_t i2c_attemps = 0;
	uint32_t i2c_status = I2C_ERROR;

	do
	{
		i2c_attemps++;
		i2c_drv_write(i2c_dev, &mfi_address, 1);
		os_thread_sleep(os_msec_to_ticks(10));		
		i2c_drv_wait_till_inactivity(i2c_dev, 10);

		// Always throw away the first write.  For whatever reason, it doesn't ever get through
		if (i2c_attemps > 1)
			i2c_drv_get_status_bitmap(i2c_dev, &i2c_status);

		if (i2c_attemps == 5)
		{
			write_message("Failure writing I2C register address");
			return;
		}
	}
	while (i2c_status != I2C_INACTIVE);

	// Read data
	uint8_t i2c_return = 0x00;
	int ret_val = i2c_drv_read(i2c_dev, &i2c_return, 1);
	if (ret_val == -WM_E_AGAIN)
		write_message("Read Timeout - Reading Register: %i", mfi_address);
	else
		write_message("\tMFI Address - %.02x, Value: %.02x", mfi_address, i2c_return);
}

//****************************************************************************
float read_mcu_temp()
{
	#if (LED_DRIVER)
		return 0;
	#endif

	// According to the MCU200 Datasheet, Section 21.4.5-Temperature Measurement
    //  - Temp in c = (ADC.RESULT.DATA[15:0] - TS_OFFSET) / TS_GAIN
    float temp_c = (float)(adc_drv_result(adc_dev) - 458) / 1.7;
    return temp_c;

}

#if (CORDELIA_BOARD)

//****************************************************************************
void motion_sensor_trip_cb()
{
	if (sensor_enabled)
		schedule_lighting_cmd(RUN_IMMEDIATELY, sensor_trip_duration_sec);
}

//****************************************************************************
void configure_cordelia(bool lighting_enable, int light_value)
{
	cancel_active_lighting_cmd();
	sensor_trip_duration_sec = light_value;
	sensor_enabled = (lighting_enable && (light_value > 0) && (light_value < 100));

	if (lighting_enable && (light_value == 100))
		set_tcr(1);
	else
		set_tcr(0);
}

#endif	// CORDELIA_BOARD
