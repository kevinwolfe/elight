#include "timers_emb.h"
#include "io.h"				// Needed for zero_crossing_count
#include <mdev_gpt.h>
#include <wm_os.h>
#include <wmtime.h>
#include <mdev_wdt.h>
#include <stdbool.h>
#include "mdev_rtc.h"
#include "main.h"

#define TRAIC_FIRE_TIMER	           	(GPT0_ID)
#define TRIAC_RECOVERY_PERIOD_USEC     	(100)

#define ALARM_TIMER	           			(GPT1_ID)
#define ALARM_DEBOUNCE_USEC	     		(1500)			// 1.5 mSec

#define LED_PWM_TIMER	           		(GPT3_ID)

#define SECOND_IN_USEC					(1000 * 1000)

static void fire_triac_callback();
static void ac_frequency_observer();

unsigned char 	locked_ac_power_frequency_hz	= 0;
unsigned int 	ac_power_period_usec 			= 0;          // Set by the 1 second timer
unsigned char 	true_ac_power_frequency_hz		= 0;
unsigned short frequency_fixed_count			= 0;

static bool		triac_on					= false;

#define RTC_TICK_PERIOD_MSEC				(1)
static volatile unsigned int two_msec_timer_rtc_val = 0;
static mdev_t 	* triac_timer_driver		= NULL;
static mdev_t 	* alarm_timer_driver		= NULL;
static mdev_t 	* led_pwm_driver			= NULL;
static mdev_t 	* wdt_dev					= NULL;
static mdev_t 	* rtc_dev					= NULL;

static volatile int watchdog_thread_statuses[NUMBER_MONITORED_THREADS];
static int 		uptime = 0;

//****************************************************************************
void initialize_timers()
{
	wmtime_init();
	gpt_drv_init(TRAIC_FIRE_TIMER);
	triac_timer_driver = gpt_drv_open(TRAIC_FIRE_TIMER);
	gpt_drv_setcb(triac_timer_driver, fire_triac_callback);

//	gpt_drv_init(ALARM_TIMER);
//	alarm_timer_driver = gpt_drv_open(ALARM_TIMER);
//	gpt_drv_setcb(alarm_timer_driver, alarm_callback);
//	gpt_drv_set(alarm_timer_driver, ALARM_DEBOUNCE_USEC);

	#if (LED_DRIVER)
		gpt_drv_init(LED_PWM_TIMER);
		led_pwm_driver = gpt_drv_open(LED_PWM_TIMER);
		set_pwm_duty(0);
		gpt_drv_start(led_pwm_driver);
	#endif

    rtc_dev = rtc_drv_open("MDEV_RTC");
    rtc_drv_start(rtc_dev);

	xTaskCreate(ac_frequency_observer, (signed char *)"freq_observer", 50, NULL, 1, NULL);
	xTaskCreate(watchdog_sitter, (signed char *)"watchdog_sitter", 50, NULL, 1, NULL);
}

//****************************************************************************
#include "uart.h"
static int duty_old = -1;
void set_pwm_duty(int duty)
{
	duty = 100 - duty;		// Invert the duty cycle to drive an n-FET

	if (duty_old != duty)
	{
		unsigned int period_ticks = 160000;		// 160000 ~= 200 Hz
		unsigned int ticks_on = (period_ticks * duty) / 100;
		unsigned int ticks_off = period_ticks - ticks_on;

		gpt_drv_pwm(led_pwm_driver, GPT_CH_0, ticks_on, ticks_off);

		// write_message("New Duty: %i %i %i", duty, ticks_on, ticks_off);
	}
	duty_old = duty;

	if (duty == 100)
		set_led_relay(0);
	else
		set_led_relay(1);
}

//****************************************************************************
void watchdog_thread_checkin(unsigned int thread_id)
{
	watchdog_thread_statuses[thread_id] = THREAD_ALIVE;
}

//****************************************************************************
void watchdog_sitter()
{
	// Setup Watchdog
	wdt_drv_init();
	wdt_dev = wdt_drv_open("MDEV_WDT");

	//  If index is entered as 15 (0xF), the actual timeout is around 90 seconds. It reduces to half, after decrementing the index by 1 everytime.
	wdt_drv_set_timeout(wdt_dev, 11);		// 5.125 seconds.  Appropriate as the HTTP requests timeout after 7.5 seconds

	int index;
	for (index = 0; index < NUMBER_MONITORED_THREADS; index++)
		watchdog_thread_statuses[index] = THREAD_INITIALIZING;

	wdt_drv_start(wdt_dev);

	while (1)
	{
		// Sleep 1/2 second
		os_thread_sleep(os_msec_to_ticks(500));

		bool pet_watchdog = true;

		for (index = 0; index < NUMBER_MONITORED_THREADS; index++)
			if (watchdog_thread_statuses[index] == THREAD_PENDING_CHECK_IN)
				pet_watchdog = false;

		if (pet_watchdog)
		{
			wdt_drv_strobe(wdt_dev);

			// Reset the status for the next iteration
			for (index = 0; index < NUMBER_MONITORED_THREADS; index++)
				if (watchdog_thread_statuses[index] == THREAD_ALIVE)
					watchdog_thread_statuses[index] = THREAD_PENDING_CHECK_IN;
		}
	}
}

//****************************************************************************
void alarm_callback()
{
	cancel_alarm();
	alarm_triggered();
}

//****************************************************************************
void start_alarm()
{
	gpt_drv_set(alarm_timer_driver, ALARM_DEBOUNCE_USEC);
	gpt_drv_start(alarm_timer_driver);
}

//****************************************************************************
void cancel_alarm()
{
	gpt_drv_stop(alarm_timer_driver);
}

//****************************************************************************
void fire_triac_callback()
{
	gpt_drv_stop(triac_timer_driver);
	triac_on = !triac_on;
	set_tcr(triac_on);

	if (triac_on)
	{
		gpt_drv_set(triac_timer_driver, TRIAC_RECOVERY_PERIOD_USEC);
		gpt_drv_start(triac_timer_driver);	
	}	
}

//****************************************************************************
void fire_triac(int usec)
{
	triac_on = false;
	gpt_drv_stop(triac_timer_driver);
	gpt_drv_set(triac_timer_driver, usec);
	gpt_drv_start(triac_timer_driver);
}

//****************************************************************************
bool zero_crossings_locked()
{
	return (frequency_fixed_count > 10);
}

//****************************************************************************
void ac_frequency_observer()
{
	while (1)
	{
		watchdog_thread_checkin(AC_FREQ_OBSERVER_THREAD);

		// Sleep 1 second
		os_thread_sleep(os_msec_to_ticks(1000));
		uptime++;

		true_ac_power_frequency_hz = zero_crossing_count / 2;
		zero_crossing_count = 0;
		
		// If we don't have a lock yet...
		if (!zero_crossings_locked())
		{
			// Bound the ac_power_frequency_hz to a reasonable value, removes noise
			if ((true_ac_power_frequency_hz >= 55) && (true_ac_power_frequency_hz <= 65))
			{
				if (locked_ac_power_frequency_hz != 60)
					   frequency_fixed_count = 0;
				else
					   frequency_fixed_count++;
					
				locked_ac_power_frequency_hz = 60;
			}
			else if ((true_ac_power_frequency_hz >= 45) && (true_ac_power_frequency_hz < 55))
			{
				if (locked_ac_power_frequency_hz != 50)
					   frequency_fixed_count = 0;
				else
					   frequency_fixed_count++;
				
				locked_ac_power_frequency_hz = 50;
			}
			else
			{
				frequency_fixed_count = 0;
				locked_ac_power_frequency_hz = 0;
			}

			if (locked_ac_power_frequency_hz > 0)
				ac_power_period_usec        = 1 * SECOND_IN_USEC / locked_ac_power_frequency_hz;
			else
				ac_power_period_usec        = 0;
		}
	}
}

//****************************************************************************
int get_uptime()
{
	return uptime;
}

//****************************************************************************
bool two_msec_timer_expired()
{
	return ((rtc_drv_get(rtc_dev) - two_msec_timer_rtc_val) >= 8);
}

//****************************************************************************
void begin_two_msec_timer()
{
	two_msec_timer_rtc_val = rtc_drv_get(rtc_dev);
}
