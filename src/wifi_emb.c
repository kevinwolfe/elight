#include "wifi_emb.h"

//#include "app_api.h"

#include "uart.h"
#include "support_funcs.h"
#include "ble.h"
#include "cloud.h"
#include "timers_emb.h"
#include "api.h"
#include "main.h"

#define WLAN_NETWORK_NAME			"SSID"
#define WIFI_THREAD_PERIOD_MS		(250)
#define WIFI_SCAN_REFRESH_RATE_S	(150)

// WIFI States
enum wifi_state_enums
{
	WIFI_STATE_UNINITIALIZED	= 0,
	WIFI_STATE_IDLE				= 1,
	WIFI_STATE_SCANNING			= 2,
	WIFI_STATE_CONNECTING		= 3,
	WIFI_STATE_CONNECTED		= 4
};

// Network Security Types
enum wifi_security_types_enums
{
	SECURITY_TYPE_WPA,
	SECURITY_TYPE_WEP,
	SECURITY_TYPE_OPEN,
	SECURITY_TYPES_COUNT,
};

#define MAX_CONNECTION_ATTEMPTS		(SECURITY_TYPES_COUNT)
#define SSID_MAX_NETWORKS			(30)

// Persistent store data struct
typedef struct
{
	char name[MAX_SSID_NAME_SIZE + 1];
	char passphrase[MAX_SSID_PASSPHRASE_SIZE + 1];
	char nvm_magic_key[4];
} wifi_connection_params_struct;

static wifi_connection_params_struct	connection_params;
static volatile enum wifi_state_enums	wifi_state;

static volatile bool	wifi_scan_completed				= false;
static volatile bool	wifi_scan_found_match			= false;
static int 				connection_attempts				= 0;
static volatile char 	ssid_list[SSID_LIST_BUFFER_SIZE];
static volatile char 	* ssid_list_end_ptr				= ssid_list;
static volatile enum 	wifi_security_types_enums ssid_list_security_types[SSID_MAX_NETWORKS];
static volatile int		ssid_list_timestamp				= 0;
static volatile bool	ssid_list_force_update 			= true;
static os_mutex_t		ssid_list_mutex;
static bool				first_connection;
static bool 			connection_params_set			= false;

char	wifi_temp_buffer[WIFI_TEMP_BUFFER_SIZE];

static int wlan_event_callback(enum wlan_event_reason event, void *data);
static void set_wifi_state(enum wifi_state_enums new_state);
static void wifi_connect();
static int wifi_scanner_callback(unsigned int count);
static void wifi_thread();


//*****************************************************************************
void initialize_wifi()
{	
	// Mark that we're uninitialized (contrary to the function name)
	// The callback from wlan_start will transition us to idle once
	// the wlan_start function calls the call_back
	set_wifi_state(WIFI_STATE_UNINITIALIZED);
	update_bt_advert_network_state_flag(WIFI_STATE_FLAG_UNINITIALIZED);
	update_bt_advert_network_error_flag(WIFI_ERROR_FLAG_CLEAR_ERRORS);

	// Enable support for compressed Wi-Fi firmware
	wlan_start(wlan_event_callback);

	#if (EXTERNAL_ANTENNA)
    	wlan_set_current_ant(2);
	#endif

	initialize_api();
	initialize_cloud();
	xTaskCreate(wifi_thread, (signed char *)"wifi_thread", 1600, NULL, 1, NULL);
}

//*****************************************************************************
static void set_wifi_state(enum wifi_state_enums new_state)
{
	switch (new_state)
	{
		case WIFI_STATE_UNINITIALIZED:
		case WIFI_STATE_IDLE:
		case WIFI_STATE_SCANNING:
			wlan_disconnect();					// Ensure we're disconnected
			break;

		case WIFI_STATE_CONNECTING:
			break;

		case WIFI_STATE_CONNECTED:
			reset_cloud_state_machine();
			break;

		default:
			update_bt_advert_network_state_flag(WIFI_STATE_FLAG_UNKNOWN_STATE);
	}

	wifi_state = new_state;
}

//*****************************************************************************
void wifi_thread()
{
	memset(&connection_params, 0, sizeof(wifi_connection_params_struct));

	if (os_mutex_create(&ssid_list_mutex, (const char*)"ssid_list_mutex", OS_MUTEX_INHERIT))
		{};

	// Pull the network from NVM if it's there
	char read_status = read_nvm_encrypted_value(NVM_NAME_WIFI_PARAMS, 						\
											   &connection_params, 				\
											   sizeof(wifi_connection_params_struct),		\
											   connection_params.nvm_magic_key);

	if (read_status == NVM_SUCCESS)
	{
		write_message("Found Network NVM");
		first_connection = false;
		connection_params_set = true;
	}
	else
	{
		update_bt_advert_provisioning_state_flag(PROVISIONING_STATE_FLAG_UNPROVISIONED);
		connection_params_set = false;
	}

	update_bt_advert_network_state_flag(WIFI_STATE_FLAG_THREAD_RUNNING);

	while (1)
	{
//		watchdog_thread_checkin(WIFI_THREAD);

		os_thread_sleep(os_msec_to_ticks(WIFI_THREAD_PERIOD_MS));

		if (!rf_device_initialized)
		{
			update_bt_advert_network_state_flag(WIFI_STATE_FLAG_WAITING_FOR_RF);
			continue;
		}

		// Scan for SSIDs while we're not connected
		switch (wifi_state)
		{
			case WIFI_STATE_UNINITIALIZED:
				update_bt_advert_network_state_flag(WIFI_STATE_FLAG_INIT);
				break;

			case WIFI_STATE_IDLE:
				// See if we should update the SSID list.  Do this
				// before we attempt any connections in case the SSID wasn't
				// seen on the last scan
				if ((ssid_list_force_update) || 	\
					((wmtime_time_get_posix() - ssid_list_timestamp) > WIFI_SCAN_REFRESH_RATE_S))
				{
					update_bt_advert_network_state_flag(WIFI_STATE_FLAG_SCANNING);
					ssid_list_force_update = false;
					set_wifi_state(WIFI_STATE_SCANNING);
					wlan_scan(wifi_scanner_callback);
					// write_message("Scanning for Wifi Networks");
				}
				// If we should attempt connecting...
				else if (connection_params_set)
				{
					update_bt_advert_network_state_flag(WIFI_STATE_FLAG_CONNECTING);
					set_wifi_state(WIFI_STATE_CONNECTING);
					wifi_connect();
				}
				else
					update_bt_advert_network_state_flag(WIFI_STATE_FLAG_IDLE);

				break;

			case WIFI_STATE_SCANNING:
				update_bt_advert_network_state_flag(WIFI_STATE_FLAG_SCANNING);
				// The callback will push the wifi_state to WIFI_STATE_CONNECTED
				break;

			case WIFI_STATE_CONNECTING:
				update_bt_advert_network_state_flag(WIFI_STATE_FLAG_CONNECTING);
				// The callback will push the wifi_state to WIFI_STATE_CONNECTED
				break;

			case WIFI_STATE_CONNECTED:
				update_bt_advert_network_state_flag(WIFI_STATE_FLAG_CONNECTED);
				check_cloud_state();			// This function is blocking!
				break;

			default:
				set_wifi_state(WIFI_STATE_IDLE);
				break;
		}
	}
}

//*****************************************************************************
void print_address(struct wlan_ip_config *addr)
{
	struct in_addr ip, gw, nm, dns1, dns2;
	char addr_type[10];

	ip.s_addr = addr->ipv4.address;
	gw.s_addr = addr->ipv4.gw;
	nm.s_addr = addr->ipv4.netmask;
	dns1.s_addr = addr->ipv4.dns1;
	dns2.s_addr = addr->ipv4.dns2;
	if (addr->ipv4.addr_type == ADDR_TYPE_STATIC)
		strncpy(addr_type, "STATIC", sizeof(addr_type));
	else if (addr->ipv4.addr_type == ADDR_TYPE_STATIC)
		strncpy(addr_type, "AUTO IP", sizeof(addr_type));
	else
		strncpy(addr_type, "DHCP", sizeof(addr_type));

	write_message("\tIPv4 Address");
	write_message("\taddress: %s", addr_type);
	write_message("\t\tIP:\t\t%s", inet_ntoa(ip));
	write_message("\t\tgateway:\t%s", inet_ntoa(gw));
	write_message("\t\tnetmask:\t%s", inet_ntoa(nm));
	write_message("\t\tdns1:\t\t%s", inet_ntoa(dns1));
	write_message("\t\tdns2:\t\t%s", inet_ntoa(dns2));

	return;
}

//*****************************************************************************
// Get Current Wifi Strengths
void get_wifi_strenth(short * rssi, int * snr)
{
	wlan_get_current_signal_strength(rssi, snr);

/*	enum wlan_ps_mode 		 	ps_mode1;
	enum wlan_uap_ps_mode 	 	ps_mode2;
	enum wlan_wfd_ps_mode 		ps_mode3;

	wlan_get_ps_mode(&ps_mode1);
	wlan_get_uap_ps_mode(&ps_mode2);
//	wlan_get_wfd_ps_mode(&ps_mode3);
	write_message("Modes: %i %i %i", ps_mode1, ps_mode2, ps_mode3);
*/
}

//*****************************************************************************
// This is a callback handler registered with wlan subsystem. This callback gets invoked on occurrence of any wlan event
int wlan_event_callback(enum wlan_event_reason event, void *data)
{
	switch (event)
	{
		case WLAN_REASON_INITIALIZED:
			set_wifi_state(WIFI_STATE_IDLE);
			break;

		case WLAN_REASON_SUCCESS:
			// Connected to a network.  Make sure we save the network credentials
			write_message("Connected!");
			struct wlan_ip_config ip_addr;
			wlan_get_address(&ip_addr);
			//print_address(&ip_addr);

			if (first_connection)
			{
				light_state_struct light_vals = {\
						.power_state	= LIGHTS_ON, \
						.target_level	= 1, \
						.pulse_lighting	= 1,
						.pulse_speed	= 4};
				set_lighting(light_vals);

				first_connection = false;
			}

			set_wifi_state(WIFI_STATE_CONNECTED);
			update_bt_advert_network_error_flag(WIFI_ERROR_FLAG_CLEAR_ERRORS);
			break;

		case WLAN_REASON_USER_DISCONNECT:
			write_message("WLAN Disconnected");
			set_wifi_state(WIFI_STATE_IDLE);
			update_bt_advert_network_error_flag(WIFI_ERROR_FLAG_NETWORK_DISCONNECT);
			break;


		case WLAN_REASON_ADDRESS_FAILED:
			write_message("Address Failed");
			update_bt_advert_network_error_flag(WIFI_ERROR_FLAG_ADDRESS_FAILED);
			set_wifi_state(WIFI_STATE_IDLE);
			break;

		case WLAN_REASON_NETWORK_AUTH_FAILED:
			write_message("Auth Failed");
			update_bt_advert_network_error_flag(WIFI_ERROR_FLAG_AUTH_FAILED);
			update_bt_advert_provisioning_state_flag(PROVISIONING_STATE_FLAG_UNPROVISIONED);
			set_wifi_state(WIFI_STATE_IDLE);
			break;

		default:
			write_message("wlan Callback %i", (int)event);
			update_bt_advert_network_error_flag(WIFI_ERROR_FLAG_UNKNOWN_CALLBACK);
			set_wifi_state(WIFI_STATE_IDLE);
			break;
	}

	return 0;
}

//*****************************************************************************
int wifi_scanner_callback(unsigned int count)
{
	// Try to find the SSID in the scan results
	int ssid_index;
	struct wlan_scan_result	scan_result;

	if (os_mutex_get(&ssid_list_mutex, os_msec_to_ticks(1)) != WM_SUCCESS)
	{
		update_bt_advert_network_error_flag(WIFI_ERROR_FLAG_SSID_LIST_LOCKED);
		goto exit_callback;
	}

	ssid_list_end_ptr = ssid_list;

	for (ssid_index = 0; ssid_index < count; ssid_index++)
	{
		wlan_get_scan_result(ssid_index, &scan_result);

		// Calculates how much space is left in our SSID list.  The null terminators must also fit in this length
		int buffer_space_remaining = SSID_LIST_BUFFER_SIZE - (ssid_list_end_ptr - ssid_list);
		if ((strlen(scan_result.ssid) > 0) && \
			((strlen(scan_result.ssid) + 1) <= buffer_space_remaining) && \
			(ssid_index < SSID_MAX_NETWORKS))
		{
			strcpy((char*)ssid_list_end_ptr, (const char*)scan_result.ssid);
			ssid_list_end_ptr += strlen((char*)ssid_list_end_ptr) + 1;

			if (scan_result.wep)
				ssid_list_security_types[ssid_index] = SECURITY_TYPE_WEP;
			else if (scan_result.wpa || scan_result.wpa2)
				ssid_list_security_types[ssid_index] = SECURITY_TYPE_WPA;
			else
				ssid_list_security_types[ssid_index] = SECURITY_TYPE_OPEN;
		}
	}

	ssid_list_timestamp = wmtime_time_get_posix();

	os_mutex_put(&ssid_list_mutex);

exit_callback:
	set_wifi_state(WIFI_STATE_IDLE);
	return 0;
}

//*****************************************************************************
// Returns the true size of the list
int copy_ssid_list(char * dest_ptr)
{

	if (os_mutex_get(&ssid_list_mutex, os_msec_to_ticks(1)) == WM_SUCCESS)
	{
		memcpy(dest_ptr, (const char*)ssid_list, SSID_LIST_BUFFER_SIZE);
		os_mutex_put(&ssid_list_mutex);

		return (ssid_list_end_ptr - ssid_list);
	}
	else
	{
		update_bt_advert_network_error_flag(WIFI_ERROR_FLAG_SSID_LIST_LOCKED);
		memset(dest_ptr, 0, SSID_LIST_BUFFER_SIZE);
		return 0;
	}
}

//*****************************************************************************
//
void set_new_wifi_ssid(char * name)
{
	connection_params_set = false;
	write_message("New WiFi SSID received - %s", name);
	strcpy(connection_params.name, name);
}

//*****************************************************************************
//
void set_new_wifi_passphrase(char * passphrase)
{
	connection_params_set = false;
	write_message("New WiFi PassPhrase received - %s", passphrase);
	strcpy(connection_params.passphrase, passphrase);
}

//*****************************************************************************
// Assumes a network scan is in progress or just completed
void wifi_connect_to_new_network()
{
	wlan_remove_network(WLAN_NETWORK_NAME);						// Always use the same name
	connection_attempts = 0;

	// Save params in NVM for future connections
	write_nvm_encrypted_value(NVM_NAME_WIFI_PARAMS, 		\
			&connection_params, 					\
			sizeof(wifi_connection_params_struct),			\
			connection_params.nvm_magic_key);

	update_bt_advert_network_error_flag(WIFI_ERROR_FLAG_CLEAR_ERRORS);
	connection_params_set = true;

	// Reset the Wifi State to begin connecting to the new network
	set_wifi_state(WIFI_STATE_IDLE);
}

//*****************************************************************************
//
void wifi_connect()
{
	if (connection_attempts >= MAX_CONNECTION_ATTEMPTS)
	{
//		update_bt_advert_network_error_flag(WIFI_ERROR_FLAG_CONNECT_FAILED);
		update_bt_advert_network_error_flag(WIFI_ERROR_FLAG_NETWORK_NOT_FOUND);
		write_message("Connection attempt failed.  Rescanning Wifi Networks");
		ssid_list_force_update = true;
		goto connect_failed;
	}

	write_message("Connecting to network");

	// Verify we see the network
	if (os_mutex_get(&ssid_list_mutex, os_msec_to_ticks(10)) != WM_SUCCESS)
	{
		update_bt_advert_network_error_flag(WIFI_ERROR_FLAG_SSID_LIST_LOCKED);
		goto connect_failed;
	}

	// Loop through our array to find the ssid in it
	char * ssid_ptr = (char*)ssid_list;
	int ssid_num = 0;
	int ssid_security_type = 0;
	bool identified_network = false;
	while ((ssid_ptr < ssid_list_end_ptr) && (ssid_num < SSID_MAX_NETWORKS))
	{
		//
		if (strcmp(connection_params.name, ssid_ptr) == 0)
		{
			identified_network = true;
			ssid_security_type = ssid_list_security_types[ssid_num];
			break;
		}

		ssid_num++;
		ssid_ptr += strlen(ssid_ptr) + 1;
	}
	os_mutex_put(&ssid_list_mutex);

	// If we can't find the SSID in the observed list, we'll just cycle through all of the different
	// Security types and hopefully hit one.
	if (!identified_network)
	{
		ssid_security_type = connection_attempts;
		write_message("SSID not found.  Attempting security type %i", ssid_security_type);
	}
	else
		write_message("SSID found in scan");

	// Don't increment connection attempts until after we've set the SSID Security Type
	connection_attempts++;

	struct wlan_network network;
	memset(&network, 0, sizeof(struct wlan_network));

	memcpy(network.name, WLAN_NETWORK_NAME, strlen(WLAN_NETWORK_NAME));		// Always use the same name
	memcpy(network.ssid, connection_params.name, strlen(connection_params.name));		// Connect to 'ssid'
	memset(network.bssid, 0, 6);					// Use any BSSID
	network.channel	= 0;							// Use any channel
	network.role	= WLAN_BSS_ROLE_STA;			// Only connect to other Access Points
	network.ip.ipv4.addr_type = ADDR_TYPE_DHCP;		// Use DHCP

	network.security.type = WLAN_SECURITY_WILDCARD;

	// Setup the pass phrases
	if (ssid_security_type == SECURITY_TYPE_WEP)
		load_wep_key((const uint8_t *)connection_params.passphrase, (uint8_t *)network.security.psk, (uint8_t *)&network.security.psk_len, sizeof(network.security.psk));
	else if (ssid_security_type == SECURITY_TYPE_WPA)
	{
		strncpy(network.security.psk, connection_params.passphrase, sizeof(network.security.psk));
		network.security.psk_len = MIN(strlen(connection_params.passphrase), sizeof(network.security.psk));
	}
	
	wlan_remove_network(WLAN_NETWORK_NAME);						// Always use the same name
	wlan_add_network(&network);

	wlan_connect(WLAN_NETWORK_NAME);
	return;

connect_failed:
	connection_attempts = 0;
	set_wifi_state(WIFI_STATE_IDLE);
	return;
}

//*****************************************************************************
char * wifi_status_msg()
{
		// Scan for SSIDs while we're not connected
		switch (wifi_state)
		{
			case WIFI_STATE_UNINITIALIZED:
				return "Wifi Uninitialized";
				
			case WIFI_STATE_IDLE:
				return "Wifi Idle";

			case WIFI_STATE_SCANNING:
				return "Wifi Scanning";

			case WIFI_STATE_CONNECTING:
				return "Wifi Connecting";

			case WIFI_STATE_CONNECTED:
				return "Wifi Connected";

			default:
				return "Wifi ERROR wifi_state!";
		}
}


//*****************************************************************************
// Assumes a network scan is in progress or just completed
void report_scanned_ssids()
{ 
	if (os_mutex_get(&ssid_list_mutex, os_msec_to_ticks(10)) != WM_SUCCESS)
		return;

	char * list_ptr = (char*)ssid_list;
	int ssid_num = 0;
	while (list_ptr < ssid_list_end_ptr)
	{
		write_message("\tSSID #%.02i - %s", ssid_num++, list_ptr);
		list_ptr += strlen(list_ptr) + 1;
	}

	os_mutex_put(&ssid_list_mutex);
}

//*****************************************************************************
bool wifi_is_connected()
{
	if (wifi_state >= WIFI_STATE_CONNECTED)
		return true;
	else
		return false;
}
