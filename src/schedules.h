#ifndef _SCHEDULES_H_
#define _SCHEDULES_H_

	#define RUN_IMMEDIATELY	(0)

	#include "light_control.h"

	void initialize_schedules();
	void check_schedules();
	void schedule_lighting_cmd(unsigned int new_start_time, unsigned int duration);
	void cancel_active_lighting_cmd();

#endif
