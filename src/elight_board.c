 #include "elight_board.h"

#include <wm_os.h>
#include <mc200_pinmux.h>
#include <mc200_i2c.h>
#include <mc200_pmu.h>
#include "main.h"

int board_console_uart_id(void)
{
	return UART0_ID;
}

int board_main_xtal()
{
	return 32000000;
}

bool board_supports_5GHz(void)
{
	return false;
}

int board_main_osc()
{
	return -WM_FAIL;
}

int board_cpu_freq()
{
	return 200000000;
}

int board_32k_xtal()
{
	return false;
}

int board_32k_osc()
{
	return false;
}

int board_rc32k_calib()
{
	return true;
}

int board_card_detect()
{
	#if (MC200_DEV_BOARD)
		GPIO_IO_Type cdlvl;

		GPIO_PinMuxFun(GPIO_9, GPIO9_GPIO9);
		GPIO_SetPinDir(GPIO_9, GPIO_INPUT);
		cdlvl = GPIO_ReadPinLevel(GPIO_9);
		if (cdlvl == GPIO_IO_HIGH)
			return false;
	#endif

	return true;
}

void board_sdio_pdn()
{
	#if (MC200_DEV_BOARD)
		GPIO_PinMuxFun(GPIO_29, GPIO29_GPIO29);
		GPIO_SetPinDir(GPIO_29, GPIO_OUTPUT);
		GPIO_WritePinOutput(GPIO_29, GPIO_IO_LOW);
	#else
		// For AW_CU277_BOARD, SDIO Pdn is on GPIO 17, GPIO 44 is unused
		GPIO_PinMuxFun(GPIO_17, GPIO17_GPIO17);
		GPIO_SetPinDir(GPIO_17, GPIO_OUTPUT);
		GPIO_WritePinOutput(GPIO_17, GPIO_IO_LOW);

		// Regular NH391, SDIO Pdn is on GPOIO 44
		//  - GPIO14 is used for MFI Reset, so
		//  - toggling it is OK and allows for code compatibility
		GPIO_PinMuxFun(GPIO_44, GPIO44_GPIO44);
		GPIO_SetPinDir(GPIO_44, GPIO_OUTPUT);
		GPIO_WritePinOutput(GPIO_44, GPIO_IO_LOW);
	#endif
}

void board_sdio_pwr()
{
	#if (MC200_DEV_BOARD)
		GPIO_WritePinOutput(GPIO_29, GPIO_IO_HIGH);
	#else // if (AW_CU277_BOARD)
		GPIO_WritePinOutput(GPIO_17, GPIO_IO_HIGH);	// CU277
		GPIO_WritePinOutput(GPIO_44, GPIO_IO_HIGH);	// NH391
	#endif
}

void board_sdio_reset()
{
	board_sdio_pdn();
	_os_delay(200);
	board_sdio_pwr();
	_os_delay(200);
}

int board_sdio_pdn_support()
{
	return true;
}


void board_gpio_power_on()
{
	/* Enable the Boot override button */
//	PMU_PowerOnVDDIO(PMU_VDDIO_D1);
	#if (MC200_DEV_BOARD)
		/* Turn On SYS_3V3_EN */
		PMU_PowerOnVDDIO(PMU_VDDIO_D0);
		GPIO_PinMuxFun(GPIO_17, GPIO17_GPIO17);
		GPIO_SetPinDir(GPIO_17, GPIO_OUTPUT);
		GPIO_WritePinOutput(GPIO_17, GPIO_IO_LOW);
	#else	// if (AW_CU277_BOARD)
		/* AON & D0 @ 3.3V */
		/* SDIO @ 1.8V */
		PMU_ConfigVDDIOLevel(PMU_VDDIO_SDIO, PMU_VDDIO_LEVEL_1P8V);
		/* WAKEUP1 is Active High */
		PMU_ConfigWakeupPin(PMU_GPIO26_INT, PMU_WAKEUP_LEVEL_HIGH);
		/* WAKEUP0 is Active low */
		PMU_ConfigWakeupPin(PMU_GPIO25_INT, PMU_WAKEUP_LEVEL_LOW);
		/* Enable the Boot override button */
		PMU_PowerOnVDDIO(PMU_VDDIO_D0);
	#endif
}

void board_uart_pin_config(int id)
{
	switch (id) {
	case UART0_ID:
		GPIO_PinMuxFun(GPIO_74, GPIO74_UART0_TXD);
		GPIO_PinMuxFun(GPIO_75, GPIO75_UART0_RXD);
		break;
	case UART1_ID:
		GPIO_PinMuxFun(GPIO_65, GPIO65_UART1_TXD);
		GPIO_PinMuxFun(GPIO_66, GPIO66_UART1_RXD);
		break;
	case UART2_ID:
	case UART3_ID:
		/* Not implemented */
		break;
	}
}

void board_i2c_pin_config(int id)
{
	switch (id) {
	case I2C0_PORT:
//		#if (AW_CU277_BOARD)
			GPIO_PinMuxFun(GPIO_44, GPIO44_I2C0_SDA);
			GPIO_PinMuxFun(GPIO_45, GPIO45_I2C0_SCL);
//		#endif
		break;
	case I2C1_PORT:
		GPIO_PinMuxFun(GPIO_4, GPIO4_I2C1_SDA);
		GPIO_PinMuxFun(GPIO_5, GPIO5_I2C1_SCL);
		break;
	case I2C2_PORT:
		GPIO_PinMuxFun(GPIO_10, GPIO10_I2C2_SDA);
		GPIO_PinMuxFun(GPIO_11, GPIO11_I2C2_SCL);
		break;
	}
}

void board_sdio_pin_config()
{
//	GPIO_PinMuxFun(GPIO_50, GPIO50_SDIO_LED);
	GPIO_PinMuxFun(GPIO_51, GPIO51_SDIO_CLK);
	GPIO_PinMuxFun(GPIO_52, GPIO52_SDIO_3);
	GPIO_PinMuxFun(GPIO_53, GPIO53_SDIO_2);
	GPIO_PinMuxFun(GPIO_54, GPIO54_SDIO_1);
	GPIO_PinMuxFun(GPIO_55, GPIO55_SDIO_0);
	GPIO_PinMuxFun(GPIO_56, GPIO56_SDIO_CMD);
}

/*
 * Application Specific APIs
 * Define these only if your application needs/uses them.
 */

int board_wifi_host_wakeup()
{
	return -WM_FAIL;
}

int board_wakeup0_functional()
{
	return false;
}

int  board_wakeup1_functional()
{
	return false;
}

int board_wakeup0_wifi()
{
	return false;
}

int board_wakeup1_wifi()
{
	return false;
}

I2C_ID_Type board_mfi_i2c_port_id()
{
	#if (MC200_DEV_BOARD)
		return I2C1_PORT;
	#else
		return I2C2_PORT;
	#endif
}

GPIO_IO_Type board_mfi_RST_pin_state()
{
	/*
	 * Return the state of the RST pin of the MFI chip. This will help
	 * our application software decide the I2C slave address of the
	 * MFI chip. Set this to appropriate value even if the RST line is
	 * not connected to any GPIO of the board.
	 */
	return GPIO_IO_HIGH;
}

int board_mfi_RST_pin_gpio_no()
{
	/*
	 * This is the GPIO pin on which the RST line is attached. If on
	 * your board the RST line is not connected to any GPIO return -1
	 * here.
	 *
	 * Note that appropriate pull-down or pull-up must be present on
	 * the RST line for the warm reset to work.
	 */
	return -1;
}
