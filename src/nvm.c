// Note, the following are defined by the wicentric make file (...\stack\build\common.mak)
// and also in settings.h (as included by wmcrypto.h).  By undefining them here, we suppress
// a GCC warning.  I suspect Marvell will fix this in the future, but until then...
#undef WMSDK_BUILD
#undef NO_DEV_RANDOM
#undef NO_PSK
#undef NO_FILESYSTEM
#undef FREERTOS
#undef CYASSL_LWIP

#include "nvm.h"

#include <stdlib.h>
#include <wmstdio.h>
#include <wm_os.h>
#include <psm.h>
#include <psm-v2.h>
#include <partition.h>
#include <wlan.h>
#include <flash.h>

#include <peripherals/spi_flash.h>
#include "uart.h"
#include "support_funcs.h"

#define ENCRYPTED_STORE_MAGIC_KEY		(0xC0FECAFE)
static unsigned char nvm_aes_initial_vector[] = {0x56, 0x2e, 0x17, 0x99,	0x6d, 0x09, 0x3d, 0x28, 0xdd, 0xb3, 0xba, 0x69, 0x5a, 0x2e, 0x6f, 0x58};

static struct partition_entry *p;
static flash_desc_t fl;
static psm_hnd_t psm_hnd;


static void check_nvm_reset_sequence();

// Persistent store data struct
typedef struct
{
	int boot_cnt;
	int nvm_reset_seqeuence_stage;
} boot_params_struct;

static boot_params_struct boot_params;

//-----------------------------------------------------------------------------
//
void initialize_nvm()
{
	// Initialize the partition module
	part_init();
	p = part_get_layout_by_id(FC_COMP_PSM, NULL);
	part_to_flash_desc(p, &fl);

	// Initilize psm module
	psm_module_init(&fl, &psm_hnd, NULL);
	
	// Drop the QPSI Clock down to 1 MHz
	PMU->PERI1_CLK_DIV.BF.QSPI1_CLK_DIV = 7;	// 3 bits wide
	QSPI1->CONF.BF.CLK_PRESCALE = 31;			// 5 bits wide

	xTaskCreate(check_nvm_reset_sequence, (signed char *)"check_nvm_reset_sequence", 300, NULL, 1, NULL);
}

//-----------------------------------------------------------------------------
//
void check_nvm_reset_sequence()
{
	if (read_nvm_value(NVM_NAME_BOOT_PARMAS, &boot_params, sizeof(boot_params_struct)) == NVM_FAILURE)
		memset(&boot_params, 0, sizeof(boot_params_struct));

	int cycle_tics = 0;
	while (1)
	{
		os_thread_sleep(os_msec_to_ticks(1000));
		cycle_tics++;

		// Don't increase the boot count until 2 seconds after booting
		if (cycle_tics == 1)
		{
			boot_params.boot_cnt++;
			boot_params.nvm_reset_seqeuence_stage++;
			write_nvm_value(NVM_NAME_BOOT_PARMAS, &boot_params, sizeof(boot_params_struct));
			write_message("Bootcount:      %d", boot_params.boot_cnt);
			write_message("Reset Sequence: %d", boot_params.nvm_reset_seqeuence_stage);
		}

		if (cycle_tics == 5)
		{
			if (boot_params.nvm_reset_seqeuence_stage >= 4)
			{
				write_message("NVM Reset Seqeuence Initiated!");
				factory_reset();
			}

			boot_params.nvm_reset_seqeuence_stage = 0;
			write_nvm_value(NVM_NAME_BOOT_PARMAS, &boot_params, sizeof(boot_params_struct));
			write_message("Reset Sequence: %d", boot_params.nvm_reset_seqeuence_stage);
		}
	}
}

//-----------------------------------------------------------------------------
//
int get_bootcount()
{
	return boot_params.boot_cnt;
}

//-----------------------------------------------------------------------------
//
bool read_nvm_value(const char * variable_name, void * value, uint32_t len)
{
	if (psm_get_variable(psm_hnd, variable_name, value, len) < 0)
		return NVM_FAILURE;

	return NVM_SUCCESS;
}


//-----------------------------------------------------------------------------
//
void write_nvm_value(const char * variable_name, const void *value, uint32_t len)
{
	char tmp[len];
	psm_get_variable(psm_hnd, variable_name, tmp, len);;

	// Write to Flash
	if (memcmp(tmp, value, len) != 0)
		if (psm_set_variable(psm_hnd, variable_name, value, len))
			write_message("Error writing %s to PSM\r\n", variable_name);
}

//*****************************************************************************
// Returns true if the decryption is successful & the magic word checks out
bool read_nvm_encrypted_value(const char * variable_name, void * value, uint32_t len, void * magic_key_ptr)
{
	uint8_t tmp_encrypted[len];

	if (read_nvm_value(variable_name, tmp_encrypted, len) == NVM_FAILURE)
		return NVM_FAILURE;

	decrypt_buffer(tmp_encrypted, value, nvm_aes_initial_vector, len);

	if (*(unsigned int*)magic_key_ptr != ENCRYPTED_STORE_MAGIC_KEY)
		return NVM_FAILURE;

	return NVM_SUCCESS;
}


//-----------------------------------------------------------------------------
//
void write_nvm_encrypted_value(const char * variable_name, const void *value, uint32_t len, void * magic_key_ptr)
{
	uint8_t tmp_encrypted[len];

	*(unsigned int*)magic_key_ptr = ENCRYPTED_STORE_MAGIC_KEY;

	encrypt_buffer(value, tmp_encrypted, nvm_aes_initial_vector, len);

	write_nvm_value(variable_name, tmp_encrypted, len);
}

//-----------------------------------------------------------------------------
//
void reset_nvm_data()
{
	psm_format(psm_hnd);
	write_message("NVM Erased");
}

//-----------------------------------------------------------------------------
const int bytes_to_test = 4;
void test_external_flash()
{
	write_message("Testing External Flash Memory");

    mdev_t *fl_dev;
	fl_dev = flash_drv_open(FL_EXT);
	if (fl_dev == NULL)
	{
		write_message("*** ERROR OPENING FLASH DRIVER ***");
		return;
	}
	
	write_message("Flash Driver opened");
	
	int index;
	uint8_t test_bytes[bytes_to_test];
	
	flash_drv_get_id(fl_dev, (uint64_t*)test_bytes);
	
	write_message("SPI Flash ID: 0x%.08X%.02X%.02X%.02X", test_bytes[3], test_bytes[2], test_bytes[1], test_bytes[0]);
	
	
	if (flash_drv_erase(fl_dev, 0, bytes_to_test) != 0)
	{
		write_message("*** ERROR ERASING FLASH ***");
		flash_drv_close(fl_dev);
		return;
	}
	
	write_message("Flash erased");
	for (index = 0; index < bytes_to_test; index++)
		test_bytes[index] = index;

	if (flash_drv_write(fl_dev, test_bytes, bytes_to_test, 0) != 0)
	{
		write_message("*** ERROR WRITING FLASH ***");
		flash_drv_close(fl_dev);
		return;
	}
	
	write_message("Flash test sequence written");
	memset((void*)test_bytes, 0, bytes_to_test);

	if (flash_drv_read(fl_dev, test_bytes, bytes_to_test, 0) != 0)
	{
		write_message("*** ERROR READING FLASH ***");
		flash_drv_close(fl_dev);
		return;
	}

	write_message("Flash test sequence read");
	int error = 0;

	for (index = 0; index < bytes_to_test; index++)
	{
		write_message("Test Sequence Byte #%.03i - Read: 0x%.02x, Expected: 0x%.02x, Match: %i", index, test_bytes[index], index & 0xFF, test_bytes[index] == (index & 0xFF));
		
		if (test_bytes[index] != (index & 0xFF))
			error = 1;
	}
	
	if (error)
		write_message("*** ERROR COMPARING FLASH TEST SEQUENCE ***");
	else
		write_message("Flash test completed successfully");

	flash_drv_close(fl_dev);	
}

//-----------------------------------------------------------------------------
void factory_reset()
{
	psm_object_delete(psm_hnd, NVM_NAME_LIGHTING_LEVELS);
	psm_object_delete(psm_hnd, NVM_NAME_WIFI_PARAMS);
	psm_object_delete(psm_hnd, NVM_NAME_CLOUD_PARAMS);

//	psm_object_delete(psm_hnd, NVM_NAME_BOOT_PARMAS);		// Do not erase boot count - Want to keep this statistic for the life of the device
	boot_params.nvm_reset_seqeuence_stage = 0;
	write_nvm_value(NVM_NAME_BOOT_PARMAS, &boot_params, sizeof(boot_params_struct));

	pm_reboot_soc();
}
