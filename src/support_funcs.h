#ifndef _SUPPORT_FUNCS_H_
#define _SUPPORT_FUNCS_H_

	
	#define MIN(a,b) (((a)<(b))?(a):(b))
	#define MAX(a,b) (((a)>(b))?(a):(b))

	// Stringify a constant
//	#define xstr(a) str(a)
//	#define str(a) #a
//	#define FW_REVISION_STRING	xstr(FW_VERSION_MAJOR) "." xstr(FW_VERSION_MINOR) "." xstr(FW_VERSION_SUBMINOR)

	// Note, the following are defined by the wicentric make file (...\stack\build\common.mak)
	// and also in settings.h (as included by wmcrypto.h).  By undefining them here, we suppress
	// a GCC warning.  I suspect Marvell will fix this in the future, but until then...
	#undef WMSDK_BUILD
	#undef NO_DEV_RANDOM
	#undef NO_PSK
	#undef NO_FILESYSTEM
	#undef FREERTOS
	#undef CYASSL_LWIP

	#include "httpc.h"

	void set_http_req_params(http_session_t http_session, http_req_t * http_req, 	\
			http_method_t http_method, char * http_resource, char * req_content, 	\
			http_hdr_field_sel_t flags);

	void set_authorization_header(http_session_t http_session, http_req_t * http_req, char * encoding_message);
	int send_request(http_session_t http_session, http_req_t * http_req, char * response_buffer, int max_response_data_size);

	#define SHA256_HASH_SIZE							(32)
	#define SHA256_BASE64_HASH_SIZE						(44)

	void decrypt_buffer(const uint8_t * cipher_text, uint8_t * plain_text, uint8_t * iv, int len);
	void encrypt_buffer(const uint8_t * plain_text, uint8_t * cipher_text, uint8_t * iv, int len);


#endif
