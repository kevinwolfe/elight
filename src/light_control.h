#ifndef _LIGHT_CONTROL_H_
#define _LIGHT_CONTROL_H_

	#include <stdbool.h>

	typedef struct
	{
		int power_state;
		int target_level;
		bool pulse_lighting;
		int pulse_speed;
		int assumed_ac_frequency;
	} light_state_struct;

    #define LINEAR_RANGE_LO_POINT  (25) 		// Best: 25
    #define LINEAR_RANGE_HI_POINT  (90) 		// Best: 100-falling edge, 95-rising edge (zero cross pulse is .75 msec, which equals 5%)

	#define LIGHTS_ON				(1)
	#define LIGHTS_OFF				(0)

    void zcr_adjust_lighting();
    void initialize_lighting();
    void set_lighting(light_state_struct new_light_state);
    void update_lighting_values_in_nvm();
    void pwm_adjust_lighting();


    extern volatile light_state_struct light_state;

#endif
