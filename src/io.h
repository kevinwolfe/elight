#ifndef _IO_H_
#define _IO_H_

	#include <stdint.h>
	#include <stdbool.h>


	enum
	{
		MFG_LED_OFF		= 1,
		MFG_LED_ON		= 0,
		MFG_LED_BLINK	= 2,
	};


	void initialize_io();
	void set_tcr(bool state);
	
	void test_mfi();
	void read_mfi_address(uint8_t mfi_address);

	float read_mcu_temp();

	void configure_cordelia(bool lighting_enable, int light_value);
	void set_mfg_green_led(char new_led_state);
	void set_mfg_red_led(char new_led_state);
	void drive_20hz();
	void get_loopbacks(char * zc_hist, char * alarm_hist);
	void alarm_triggered();
	void set_pwm(int duty);
	void set_led_relay(int state);
	float read_adc();

    extern volatile int 	zero_crossing_count;
    extern volatile bool	lighting_control_enabled;

#endif
