#ifndef __CLOUD_H_
#define __CLOUD_H_

// Note, the following are defined by the wicentric make file (...\stack\build\common.mak)
// and also in settings.h (as included by wmcrypto.h).  By undefining them here, we suppress
// a GCC warning.  I suspect Marvell will fix this in the future, but until then...
#undef WMSDK_BUILD
#undef NO_DEV_RANDOM
#undef NO_PSK
#undef NO_FILESYSTEM
#undef FREERTOS
#undef CYASSL_LWIP

	#include "httpc.h"
	#include "nvm.h"
	
	// Below are needed by BT interfaces
	#define MAX_API_ID_STR_SIZE					(18)	// Example: 322548458554655871
	#define MAX_API_CONNECTION_TOKEN_STR_SIZE	(32)	// Example: cad7c45b3b7a40aaad4bb5e90e5552b3
	#define MAX_API_SIGNING_KEY_STR_SIZE		(32)	// Example: d66e2f6583ddab218a4df2c683196ad0

	// WIFI States
	enum cloud_state_enums
	{
		CLOUD_STATE_FLAG_UNKNOWN_STATE,
		CLOUD_STATE_IDLE,
		CLOUD_STATE_UNPROVISIONED,
		CLOUD_STATE_MISSING_SIGNING_KEY,
		CLOUD_STATE_MISSING_PUBNUB_PARAMS,
		CLOUD_STATE_WAITING_FOR_API_SYNC,
		CLOUD_STATE_PUBNUB_CLOSED,
		CLOUD_STATE_PUBNUB_IDLE,
		CLOUD_STATE_PUBNUB_OPEN,
	};

	enum cloud_error_flags_enums
	{
		CLOUD_ERROR_FLAG_CLEAR_ERRORS			= 0,
		CLOUD_ERROR_FLAG_UNKNOWN_ERROR			= (1 << 0),
	};

	enum provisioning_state_flags_enums
	{
		PROVISIONING_STATE_FLAG_UNKNOWN_STATE	= 0x00,
		PROVISIONING_STATE_FLAG_UNPROVISIONED	= 0x01,
		PROVISIONING_STATE_FLAG_PENDING			= 0x02,
		PROVISIONING_STATE_FLAG_PROVISIONED		= 0x10,
	};

	enum provisioning_error_flags_enums
	{
		PROVISIONING_ERROR_FLAG_CLEAR_ERRORS	= 0,
		PROVISIONING_ERROR_FLAG_UNKNOWN_ERROR	= (1 << 0),
	};


    void initialize_cloud();
    void reset_cloud_state_machine();

    void check_cloud_state();
    void check_cloud_provisioning();
    void register_new_cloud_params(char * new_api_id, char * new_api_connection_token);

    void close_pubnub_session();
    char * cloud_status_msg();

	char * get_api_id();
	char * get_singing_key();

	void deprovision_from_cloud();

	bool cloud_is_provisioned();

	void cloud_handle_new_system_time(int time_change_s);

#endif
