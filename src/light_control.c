#include "light_control.h"
#include "api.h"
#include "nvm.h"
#include "uart.h"
#include "io.h"
#include "uart.h"
#include "timers_emb.h"
#include "support_funcs.h"
#include "schedules.h"
#include "main.h"

void adjust_target_lighting_levels();

#define LIGHTING_NVM_UPDATE_DELAY_TIME_S			(8)		// Wait 5 seconds before saving value to nvm.  Helps remove unnecessary writes.
static int brightness_adjustment_delta              = 1;

volatile light_state_struct light_state = {LIGHTS_ON, 10, 1, 1, 60};

static unsigned short pulse_pause_cnt = 0;
static volatile int actual_dimming_level = 0;
static volatile bool lighting_initialized = false;
static int		write_lighting_nvm_timemark			= -1;

/*****************************************************************************/
void initialize_lighting()
{
	// Read Cloud Provisioning Data from NVM
	light_state_struct init_light_state;
	char read_success = read_nvm_value(NVM_NAME_LIGHTING_LEVELS, &init_light_state, sizeof(light_state_struct));

	if (read_success == NVM_FAILURE)
	{
		// Only run fading demo for production units.  During production testing,
		// The light bulb state is used to indicate status.  Once the tests are all complete
		// a new production firmware build is loaded and the bulbs will begin to fade
		// indicating to the test operator that the bulbs are fully functional and the tests pass
		#if (PRODUCTION_BUILD)
			init_light_state.power_state			= LIGHTS_ON;
			init_light_state.target_level			= 0;
			init_light_state.pulse_lighting			= 1;
			init_light_state.pulse_speed			= 7;
			init_light_state.assumed_ac_frequency	= 60;
		#else
			init_light_state.power_state			= LIGHTS_ON;
			init_light_state.target_level			= 100;
			init_light_state.pulse_lighting			= 0;
			init_light_state.pulse_speed			= 7;
			init_light_state.assumed_ac_frequency	= 60;
		#endif

		write_lighting_nvm_timemark = wmtime_time_get_posix();
	}

	if (init_light_state.assumed_ac_frequency < 55)
		ac_power_period_usec = 20000;		// 1 / (50 Hz)
	else
		ac_power_period_usec = 16667;		// 1 / (60 Hz)

	set_lighting(init_light_state);

	write_message("Let there be light!");
	lighting_initialized = true;
}

/*****************************************************************************/
char actual_adjust_rate = 1;
void zcr_adjust_lighting()
{
	if (!lighting_initialized)
	{
		set_tcr(0);
		return;
	}

	#if (CORDELIA_BOARD)
		return;
	#endif

	adjust_target_lighting_levels();

	// If we're supposed to be off, just kill the TCR and punt.  No need to do anything else
    if (actual_dimming_level == 0)
    {
        set_tcr(0);
        return;
    }

	// Make sure that we have a reasonable estimate for the ac power period
	if (ac_power_period_usec < 6000)
		return;

	// OK, we're dimming.  Kick off the timer accordingly
	// Divide by two, since we're dealing with only half an AC Cycle
	unsigned int triac_period_usec = ac_power_period_usec / 2;
	int triac_linear_lo_point_usec = (signed)(triac_period_usec / 100) * LINEAR_RANGE_LO_POINT;
	int triac_linear_hi_point_usec = (signed)(triac_period_usec / 100) * LINEAR_RANGE_HI_POINT;
	int triac_linear_step_val = (triac_linear_hi_point_usec - triac_linear_lo_point_usec) / 100;

	int hold_triac_off_time_usec = triac_period_usec - triac_linear_lo_point_usec - (triac_linear_step_val * actual_dimming_level);

	fire_triac(hold_triac_off_time_usec);
}

/*****************************************************************************/
void adjust_target_lighting_levels()
{
    if (light_state.pulse_lighting)
    {
        pulse_pause_cnt++;
        if (pulse_pause_cnt >= light_state.pulse_speed)
        {
            pulse_pause_cnt = 0;

            // Make sure we're starting off at a real demo point
            if (light_state.target_level > 0x63)
                light_state.target_level = 0x63;
            if (light_state.target_level < 0x01)
                light_state.target_level = 0x01;
            
            // Toggle our dimming direction.  Don't allow the lighting value to hit zero or 100
            if ((light_state.target_level ==  0x63) || (light_state.target_level == 1))
                brightness_adjustment_delta *= -1;
            
            light_state.target_level += brightness_adjustment_delta;
        }
    }
    
    // Move to our target gradually
	if (actual_dimming_level < light_state.target_level)
		actual_dimming_level += MIN(actual_adjust_rate + 1, light_state.target_level - actual_dimming_level);
	else if (actual_dimming_level > light_state.target_level)
		actual_dimming_level -= MIN(actual_adjust_rate + 1, actual_dimming_level - light_state.target_level);

	// Set actual_dimming to 0 if powerstate is off.  This resolves an issue where if the power
	// state turns back on, we want to start ramping up from off, not the last used dim level.
	if (light_state.power_state == LIGHTS_OFF)
		actual_dimming_level = 0;

    if (actual_dimming_level > 100)
    	actual_dimming_level = 100;
}

/*****************************************************************************/
void pwm_adjust_lighting()
{
	if (!lighting_initialized)
	{
		set_pwm_duty(0);
		return;
	}

	#if (CORDELIA_BOARD)
		return;
	#endif

	adjust_target_lighting_levels();

	set_pwm_duty(actual_dimming_level);
}

/*****************************************************************************/
void set_lighting(light_state_struct new_light_state)
{
	#if (CORDELIA_BOARD)
		return;
	#endif

	// Save the lighting value request first before we potentially modify it
	if (new_light_state.pulse_lighting)
        new_light_state.target_level = actual_dimming_level;

	if (new_light_state.target_level > 100)
			new_light_state.target_level = 100;

	cancel_active_lighting_cmd();
    light_state = new_light_state;

    update_cloud_lighting_status(new_light_state);

    write_lighting_nvm_timemark = wmtime_time_get_posix() + LIGHTING_NVM_UPDATE_DELAY_TIME_S;
}

/*****************************************************************************/
void update_lighting_values_in_nvm()
{
	if ((write_lighting_nvm_timemark >= 0) && (write_lighting_nvm_timemark <= wmtime_time_get_posix()))
	{
		light_state.assumed_ac_frequency = locked_ac_power_frequency_hz;

		write_nvm_value(NVM_NAME_LIGHTING_LEVELS, 			\
				(void*)&light_state,			 					\
				sizeof(light_state_struct));

		write_lighting_nvm_timemark = -1;
	}
}
