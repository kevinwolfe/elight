#ifndef _BLE_H_
#define _BLE_H_

	#define BT_CONNECTION_OPENED	(1)
	#define BT_CONNECTION_CLOSED	(0)

	// Note: BT.c provides the device ID as it's based off of the BT MAC address
	#define PHYSICAL_DEVICE_ID_SIZE	(6)

	#define BT_DEVICE_NAME		"Elight"

	# pragma pack (1)
	typedef struct
	{
		const char fw_revision[3];
		char device_id[PHYSICAL_DEVICE_ID_SIZE];
		char network_state_flag;
		char network_error_flag;
		char cloud_state_flag;
		char cloud_error_flag;
		char provisioning_state_flag;
		char provisioning_error_flag;
	} elight_mfg_data_struct;
	# pragma pack ()

	void initialize_bluetooth();
	void check_ble_advertisement_state();
	
	char simple_substitution_cypher(char input, char key_1, char key_2);
	char * get_elight_id();

	void update_bt_advert_network_state_flag(char new_state_flag);
	void update_bt_advert_network_error_flag(char new_error_flag);
	void update_bt_advert_cloud_state_flag(char new_state_flag);
	void update_bt_advert_cloud_error_flag(char new_error_flag);
	void update_bt_advert_provisioning_state_flag(char new_state_flag);
	void update_bt_advert_provisioning_error_flag(char new_error_flag);

	extern elight_mfg_data_struct * elight_mfg_data_ptr;
	extern bool rf_device_initialized;

#endif /* _BLE_H_ */
