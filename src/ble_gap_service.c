#include "mrvlstack_config.h"
#include "utils.h"
#include "main_gatt_api.h"
#include "ble_gap_service.h"
//#include "oss.h"
#include <string.h>
#include "uart.h"
#define APPEARANCE_VAL_LEN 0x02

uint8 device_name[] = "elight";
uint8 appearance_val[APPEARANCE_VAL_LEN] = {0x00,0x00};		// Unknown Appearance

static tgatts_rsp			gatts_response;

static boolean 				gap_srvc_initialized = FALSE;

static tgatt_uuid16_char_decl_record			CharDecl_Devicename;
static tgatt_uuid16_charval_decl_record_std		CharVal_Devicename;
static tgatt_uuid16_char_decl_record			CharDecl_Appearance;
static tgatt_uuid16_charval_decl_record_std		CharVal_Appearance;

static tgatts_cb_result gap_srvc_req_handler(tconn_id conn_id, tgatt_event_ID req_type , tgatts_event_param *p_req_data);


//-----------------------------------------------------------------------------
// START Primary service - GAP Service 
tgatt_uuid16_service_record GAPServiceRecord =
{
	// Service Definition
	NULL,                                                   ///< Link pointer for all_service_q 
	{&CharDecl_Devicename, GATT_PRIM_SERVICE_RECORD_UUID16, GATT_START_HANDLE},    ///< record header.next_ptr is set to address of next record of the service 
	4,                                                      ///< Service End handle, set this value to total number of attributes associated with this service excluding service record itself
	UUID_DECL_PRIMARY_SERVICE,                              ///< 0x2800 for «Primary Service» 
	gap_srvc_req_handler,                                   ///< Service Callback
	UUID_SERVICE_GAP,                                       ///< 0x1800 - for Bluetooth GAP service 
};

static tgatt_uuid16_char_decl_record CharDecl_Devicename =
{
	// Characteristics Declaration
	{&CharVal_Devicename, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},        ///< record header.next_ptr is set to address of next record of the service 
	UUID_DECL_CHARACTERISTIC,                               ///< 0x2803 for «Characteristics Declaration» 
	GATT_CH_PROP_READ,                                      ///< Read 
	GATT_NEXT_HANDLE,                                       ///< 
	UUID_CIC_DEVICE_NAME                                    ///< UUID for Device Name 
};

static tgatt_uuid16_charval_decl_record_std CharVal_Devicename =
{
	// Characteristics Value Declaration
	{&CharDecl_Appearance, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},     ///< record header.next_ptr is set to address of next record of the service 
	UUID_CIC_DEVICE_NAME,                                                        ///< UUID for Device Name
	GATT_PERM_READ_NO_SECURITY_LEVEL | GATT_PERM_WRITE_NO_SECURITY_LEVEL
};

static tgatt_uuid16_char_decl_record CharDecl_Appearance =
{
	// Characteristics Declaration
	{&CharVal_Appearance, GATT_CHAR_DECL_RECORD_UUID16, GATT_AUTO_HANDLE},            ///< record header.next_ptr is set to address of next record of the service 
	UUID_DECL_CHARACTERISTIC,                               ///< 0x2803 for «Characteristics Declaration» 
	GATT_CH_PROP_READ,                                      ///< Read
	GATT_NEXT_HANDLE,                                       ///< It will be set to handle of CharVal_BatteryLevel attribute record
	UUID_CIC_APPEARANCE                                     ///< UUID for Appearance 
};

static tgatt_uuid16_charval_decl_record_std CharVal_Appearance =
{
	// Characteristics Value Declaration
	{NULL, GATT_CHARVAL_DECL_RECORD_UUID16_STD, GATT_AUTO_HANDLE},       ///< record header.next_ptr is set to address of next record of the service 
	UUID_CIC_APPEARANCE,                                                 ///< UUID for Appearance
	GATT_PERM_READ_NO_SECURITY_LEVEL | GATT_PERM_WRITE_NO_SECURITY_LEVEL
};
// END Primary service - GAP Service 

//-----------------------------------------------------------------------------
void gap_serv_init(void)
{
	// Add the GAP service record to GATT Only once, as there can be only single instance for Generic Access Service
	if (!gap_srvc_initialized)
	{
		// Create GAP Service
		GATTS_Add_Service((tgatt_service_record_common *) &GAPServiceRecord);
		gap_srvc_initialized = TRUE;
	}
	return;
}

//-----------------------------------------------------------------------------
// Service Request Handler Registered with Local Gatt Server
// This Callback funtion handles all the Remote Gatt Client Requests & Events of this service
static tgatts_cb_result gap_srvc_req_handler(tconn_id conn_id, tgatt_event_ID req_type , tgatts_event_param *p_req_data)
{
	tgatts_cb_result result;

	if(req_type == GATT_EVENT_READ_REQ) 
	{
		result.status = ATT_SUCCESS;
		
		if (p_req_data->request_param.handle == CharVal_Devicename.header.handle)
		{
			gatts_response.val_len = strlen((const char *)device_name);
			gatts_response.p_val = device_name;
		}
		else if (p_req_data->request_param.handle == CharVal_Appearance.header.handle)
		{
			gatts_response.val_len = APPEARANCE_VAL_LEN;
			gatts_response.p_val = appearance_val;
		}
		else
			result.status = ATT_ATTR_NOT_FOUND;

		gatts_response.rsp_of = GATT_EVENT_READ_REQ;
		gatts_response.handle = p_req_data->request_param.handle;
		GATTS_Send_Rsp(conn_id, result.status, &gatts_response); 
	}

	return result;  
}
