#ifndef _TIMERS_EMB_H_
#define _TIMERS_EMB_H_

	#include <stdbool.h>

	void watchdog_sitter();
	void watchdog_thread_checkin(unsigned int thread_id);
    void initialize_timers();
    void fire_triac(int usec);
    int get_uptime();

    void alarm_callback();
    void start_alarm();
    void cancel_alarm();

    bool two_msec_timer_expired();
    void begin_two_msec_timer();
    bool zero_crossings_locked();
    void set_pwm_duty(int duty);

	// WIFI States
	enum watchdog_monitored_threads_enums
	{
		MAIN_SYSTEM_THREAD,
		WIFI_THREAD,
		AC_FREQ_OBSERVER_THREAD,
		API_IF_THREAD,
		NUMBER_MONITORED_THREADS,
	};
    
	// WIFI States
	enum watchdog_threads_states_enums
	{
		THREAD_INITIALIZING,
		THREAD_PENDING_CHECK_IN,
		THREAD_ALIVE,
	};

    extern unsigned char locked_ac_power_frequency_hz;
    extern unsigned char true_ac_power_frequency_hz;
    extern unsigned int ac_power_period_usec;

#endif
