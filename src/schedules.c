#include <wm_os.h>
#include <wmtime.h>
#include "io.h"
#include "main.h"
#include "uart.h"
#include "timers_emb.h"
#include "schedules.h"

unsigned int 		cmd_start_time;
unsigned int 		cmd_stop_time;
static os_mutex_t	schedules_mutex;

//****************************************************************************/
void initialize_schedules()
{
	cmd_start_time = (unsigned int)-1;
	cmd_stop_time = (unsigned int)-1;
	if (os_mutex_create(&schedules_mutex, (const char*)"schedules_mutex", OS_MUTEX_INHERIT))
		{};
}

//****************************************************************************/
void check_schedules()
{
	if (os_mutex_get(&schedules_mutex, os_msec_to_ticks(1)) != WM_SUCCESS)
		return;

	if (cmd_start_time <= wmtime_time_get_posix())
	{
		cmd_start_time = (unsigned int)-1;
		set_tcr(1);
	}

	if (cmd_stop_time <= wmtime_time_get_posix())
	{
		cmd_stop_time = (unsigned int)-1;
		set_tcr(0);
	}
	os_mutex_put(&schedules_mutex);
}

//****************************************************************************/
void schedule_lighting_cmd(unsigned int new_start_time, unsigned int duration)
{
	if (os_mutex_get(&schedules_mutex, os_msec_to_ticks(5)) != WM_SUCCESS)
		return;

	if (new_start_time == RUN_IMMEDIATELY)
		new_start_time = wmtime_time_get_posix();

	cmd_start_time = new_start_time;
	cmd_stop_time = new_start_time + duration;
	os_mutex_put(&schedules_mutex);
}

//****************************************************************************/
void cancel_active_lighting_cmd()
{
	if (os_mutex_get(&schedules_mutex, os_msec_to_ticks(5)) != WM_SUCCESS)
		return;

	if (cmd_stop_time != (unsigned int)-1)
		cmd_stop_time = (unsigned int)-1;

	os_mutex_put(&schedules_mutex);
}
