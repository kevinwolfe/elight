#ifndef __API_H_
#define __API_H_

	#include "light_control.h"
	#include "main.h"

	void update_cloud_lighting_status(light_state_struct light_state);

	void initialize_api();
	void force_api_update();
	bool api_sync_complete();
	bool received_api_time();
	long long load_active_pubnub_timemark();
	void reset_pubnub_timemark();
	void register_new_pubnub_timemark(long long new_pubnub_timemark, bool update_api);
	void force_time_sync();

	#if (PRODUCTION_BUILD)
		#define API_DOMAIN		"https://api.elight.co"
	#else
		#define API_DOMAIN		"https://api-staging.elight.co"
	#endif

#endif
