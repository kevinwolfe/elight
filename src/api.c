#include "api.h"
#include "timers_emb.h"
#include "cloud.h"
#include "uart.h"
#include "wifi_emb.h"
#include "jsonv2.h"
#include "support_funcs.h"
#include "fw_version_info.h"
#include "io.h"
#include "update_fw.h"

#define API_THREAD_PERIOD_MS				(1 * 1000)
#define API_TEMP_BUFFER_SIZE				(192)
#define API_GET_TIMEOUT_MS					(1000)
#define JSON_PARSER_MAX_TOKENS     			10
#define API_TIME_SYNC_RATE_S				0xFFFF 			// Update approximately once every 18 hours
#define API_STATUS_UPDATE_RATE_S			(60 * 60 * 4) 	// Update approximately once every 4 hours
#define API_STATUS_UPDATE_DELAY_TIME_S		(8)

#define HTTP_RESPONSE_BAD_REQUEST							(400)
#define HTTP_RESPONSE_UNAUTHORIZED							(401)
#define HTTP_RESPONSE_DEVICE_REMOVED_FROM_API_SERVER		(410)

static int 					next_api_timecheck;
static light_state_struct	lighting_values;
static int 					next_api_server_update;
static bool					updated_api_server = false;
static long long 			active_pubnub_time = 0;

static jobj_t 				json_parser;
static jsontok_t			json_tokens[JSON_PARSER_MAX_TOKENS];
static char 				api_temp_buffer[API_TEMP_BUFFER_SIZE];
static bool					forced_time_sync_attempted = false;

bool sync_to_api_time();
bool update_api_server();
void api_thread();

//*****************************************************************************
//
void initialize_api()
{
	xTaskCreate(api_thread, (signed char *)"api_if", 1600, NULL, 1, NULL);
}

//*****************************************************************************
//
void api_thread()
{
	next_api_timecheck = 0;
	next_api_server_update = 0;

	while (1)
	{
		os_thread_sleep(os_msec_to_ticks(API_THREAD_PERIOD_MS));

		if (wifi_is_connected())
		{
			sync_to_api_time();
			update_api_server();
		}

//		watchdog_thread_checkin(API_IF_THREAD);
	}
}

/*****************************************************************************/
//
void force_time_sync()
{
	next_api_timecheck = 0;
}

/*****************************************************************************/
// Return status indicates whether an attempt was connected (e.g. whether we need
// to get back and pet the watchdog)
bool sync_to_api_time()
{
	char url_str[] = API_DOMAIN;
	http_req_t http_req;
	http_session_t http_session = 0;
	char sync_success = false;

	if (wmtime_time_get_posix() < next_api_timecheck)
		goto function_exit;

	// Update the sys time
	write_message("Requesting API Server Time");

//	const httpc_cfg_t httpc_config = {.flags = 0, .retry_cnt = 5, .ctx = NULL};
	// Initiate a connection with the remote Web Server using a call to http_open_session().
	int status = http_open_session_with_rcv_timeout(&http_session, url_str, NULL, API_GET_TIMEOUT_MS);
	if (status != WM_SUCCESS)
	{
		write_message("Error getting time");
		goto function_exit;
	}

	strcpy(api_temp_buffer, API_DOMAIN "/time");

	// Setup the parameters for the HTTP Request. This only prepares the request to be sent out
	set_http_req_params(http_session, &http_req, HTTP_GET, api_temp_buffer, NULL, STANDARD_HDR_FLAGS);

	if (send_request(http_session, &http_req, api_temp_buffer, API_TEMP_BUFFER_SIZE) != WM_SUCCESS)
		goto function_exit;

	// Initialize the JSON parser and parse the given string
	int error = json_init(&json_parser, json_tokens, JSON_PARSER_MAX_TOKENS, api_temp_buffer, strlen(api_temp_buffer));
	if (error != WM_SUCCESS)
	{
		write_message("JSON Error: %i", error);
		goto function_exit;
	}

	// Get the value of "now" which is a string of the time value
	int32_t new_sys_time = 0;
	if (json_get_val_int(&json_parser, "now", &new_sys_time) == WM_SUCCESS)
	{
		int old_sys_time = wmtime_time_get_posix();
		// API Time is in seconds
		wmtime_time_set_posix(new_sys_time);
		write_message("Time: %i", wmtime_time_get_posix());
		cloud_handle_new_system_time(new_sys_time - old_sys_time);
	}
	else
		goto function_exit;

	// Calculate when we should perform the next time check
	int current_time = wmtime_time_get_posix();

	// If we're provisioned, artificially roll back the current
	// time to synchronize all updates with the the lower short of the device ID.
	// This will mean all devices will update more or less randomly throughout any
	// given 18 hour period
//	if (cloud_data.state >= CLOUD_STATE_PUBNUB_CLOSED)
	{
		unsigned int time_of_day_to_update = (*(unsigned int *)get_api_id()) % API_TIME_SYNC_RATE_S;
		int current_time_adjustment = current_time - time_of_day_to_update;
		current_time -= (current_time_adjustment % API_TIME_SYNC_RATE_S);
	}

	next_api_timecheck = current_time + API_TIME_SYNC_RATE_S;

	sync_success = true;

function_exit:
	if (http_session != 0)
		http_close_session(&http_session);

	return sync_success;
}

/*****************************************************************************/
// Return status indicates whether an attempt was connected (e.g. whether we need
// to get back and pet the watchdog)
bool update_api_server()
{
	// Make sure we're provisioned and have a valid ID
	if (!cloud_is_provisioned())
		return false;

	if (wmtime_time_get_posix() < next_api_server_update)
		return false;

	// Only update the api server if we've successfully gotten the api timestamp
	if (!received_api_time())
		return false;

	updated_api_server = false;			// Mark that the update wasn't successful yet

	// Always delay an extra second to allow all of the updates to all
	// parameters to get on through.  This should minimize back-to-back updates
	os_thread_sleep(os_msec_to_ticks(API_THREAD_PERIOD_MS));

	int request_time = wmtime_time_get_posix();

	// Update the sys time
	write_message("Updating API state");

	char url_str[] = API_DOMAIN;

	http_req_t http_req;
	http_session_t http_session = 0;

	// Initiate a connection with the remote Web Server using a call to http_open_session().
	// int status = http_open_session(&http_session, wifi_temp_buffer, 0, NULL, 0);
	int status = http_open_session_with_rcv_timeout(&http_session, url_str, NULL, API_GET_TIMEOUT_MS);
	if (status != WM_SUCCESS)
		goto function_exit;

	/*
		 - Device Boot Count ('dev_boot_cnt', single integer)
		 - Device Run Time ('dev_run_time', single integer, in seconds)
		 - Firmware Revision ('fw_rev', string: int.int.int)
		 - Measured MCU Temperature ('mcu_tmp', single integer, in Fahrenheit)
		 - Device's Last PubNub Timestamp Used ('pubnub_time', string, optional value)
	*/

	// short wifi_rssi;
	// int wifi_snr;
	// get_wifi_strenth(&wifi_rssi, &wifi_snr);

	char msg_body[API_TEMP_BUFFER_SIZE];

	if (active_pubnub_time)
		snprintf(msg_body, API_TEMP_BUFFER_SIZE, "{\"%s\":%s,\"%s\":%i,\"%s\":%i,\"%s\":%i,\"%s\":\"%.02i.%.02i.%.02i\",\"%s\":%i,\"%s\":\"%lld\"}", 	\
				"on",			light_state.power_state ? "true" : "false",					\
				"dim",			light_state.target_level,									\
				"boot_cnt",		get_bootcount(),											\
				"uptime",		get_uptime(),												\
				"fw_rev",		FW_VERSION_MAJOR, FW_VERSION_MINOR, FW_VERSION_SUBMINOR,	\
				"mcu_temp",		(int)read_mcu_temp(),										\
				"pubnub_time",	active_pubnub_time);
	else
		snprintf(msg_body, API_TEMP_BUFFER_SIZE, "{\"%s\":%s,\"%s\":%i,\"%s\":%i,\"%s\":%i,\"%s\":\"%.02i.%.02i.%.02i\",\"%s\":%i}", 					\
				"on",			light_state.power_state ? "true" : "false",					\
				"dim",			light_state.target_level,									\
				"boot_cnt",		get_bootcount(),											\
				"uptime",		get_uptime(),												\
				"fw_rev",		FW_VERSION_MAJOR, FW_VERSION_MINOR, FW_VERSION_SUBMINOR,	\
				"mcu_temp",		(int)read_mcu_temp());

/*	snprintf(msg_body, API_TEMP_BUFFER_SIZE, "{%s:%s,%s:%i}", 	\
			"\"on\"",			light_state.power_state ? "true" : "false",	\
			"\"dim\"",			light_state.target_level);

	write_message("Msg Post: %s", msg_body);
*/

	snprintf(api_temp_buffer, API_TEMP_BUFFER_SIZE, 					\
			API_DOMAIN "/device/%s/currentState?request_time=%i",		\
			get_api_id(), request_time);

	// Setup the parameters for the HTTP Request. This only prepares the request to be sent out
	set_http_req_params(http_session, &http_req, HTTP_POST, api_temp_buffer, msg_body, STANDARD_HDR_FLAGS);

	// Create the Authentication Message
	snprintf(api_temp_buffer, API_TEMP_BUFFER_SIZE, 			\
			"%i/device/%s/currentState?request_time=%i%s",		\
			request_time, get_api_id(),  \
			request_time, msg_body);

/*	char temp = api_temp_buffer[73];
	api_temp_buffer[73] = 0;
	write_message("Auth: %s", api_temp_buffer);
	api_temp_buffer[73] = temp;
	write_message("Auth: %s", api_temp_buffer + 73);
*/
	set_authorization_header(http_session, &http_req, api_temp_buffer);

	int request_response = send_request(http_session, &http_req, api_temp_buffer, API_TEMP_BUFFER_SIZE);
	switch (request_response)
	{
		case HTTP_RESPONSE_BAD_REQUEST:
		case HTTP_RESPONSE_UNAUTHORIZED:
			if (!forced_time_sync_attempted)
			{
				force_time_sync();
				forced_time_sync_attempted = true;
			}

			goto function_exit;

		case WM_SUCCESS:
			forced_time_sync_attempted = false;
			break;

		case HTTP_RESPONSE_DEVICE_REMOVED_FROM_API_SERVER:
			factory_reset();				// This will reboot the device

		default:
			goto function_exit;
	}

	// The /device/id/currentState method will return pubnub time and firmware update url.
	// Url is only if the device needs to update itself.

	// Initialize the JSON parser and parse the given string

	int error = json_init(&json_parser, json_tokens, JSON_PARSER_MAX_TOKENS, api_temp_buffer, strlen(api_temp_buffer));
	if (error != WM_SUCCESS)
	{
		write_message("JSON Error: %i", error);
		goto function_exit;
	}

	// Get the value of "now" which is a string of the time value
	if (json_get_val_str(&json_parser, "update_url", api_temp_buffer, API_TEMP_BUFFER_SIZE) == WM_SUCCESS)
		run_fw_update(api_temp_buffer);		// Update the device!

	// Get the value of "now" which is a string of the time value
	if (json_get_val_str(&json_parser, "pubnub_time", api_temp_buffer, API_TEMP_BUFFER_SIZE) == WM_SUCCESS)
		register_new_pubnub_timemark(atoll(api_temp_buffer), false);

	updated_api_server = true;
	next_api_server_update = wmtime_time_get_posix() + API_STATUS_UPDATE_RATE_S;

function_exit:
	if (http_session != 0)
		http_close_session(&http_session);

	return true;
}

/*****************************************************************************/
void update_cloud_lighting_status(light_state_struct light_state)
{
	if (memcmp(&light_state, &lighting_values, sizeof(light_state_struct)) != 0)
	{
		lighting_values = light_state;
		next_api_server_update = wmtime_time_get_posix() + API_STATUS_UPDATE_DELAY_TIME_S;
	}
}

/*****************************************************************************/
void force_api_update()
{
	next_api_server_update = wmtime_time_get_posix();
}

/*****************************************************************************/
bool received_api_time()
{
	return (next_api_timecheck > 0);
}

/*****************************************************************************/
bool api_sync_complete()
{
	return updated_api_server;
}

/*****************************************************************************/
long long load_active_pubnub_timemark()
{
	return active_pubnub_time;
}

/*****************************************************************************/
void reset_pubnub_timemark()
{
	active_pubnub_time = 0;
}

/*****************************************************************************/
void register_new_pubnub_timemark(long long new_pubnub_timemark, bool update_api)
{
	if (new_pubnub_timemark > active_pubnub_time)
	{
		active_pubnub_time = new_pubnub_timemark;

		if (update_api)
			force_api_update();
	}
}
