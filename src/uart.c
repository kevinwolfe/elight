#include <stdarg.h>
#include <cli.h>
#include <wmtime.h>

#include "elight_board.h"
#include "uart.h"
#include "light_control.h"
#include "wifi_emb.h"
#include "nvm.h"
#include "io.h"
#include "fw_version_info.h"
#include "update_fw.h"
#include "nvm.h"
#include "main.h"
#include "timers_emb.h"
#include "cloud.h"

// CLI command wrappers
static void cli_alarm_sim(int argc, char **argv);
static void cli_reset_device(int argc, char **argv);
static void cli_erase_nvm(int argc, char **argv);
static void cli_dim_lighting(int argc, char **argv);
static void cli_wifi_connect(int argc, char **argv);
static void cli_test_mfi(int argc, char **argv);
static void cli_system_status(int argc, char **argv);
static void cli_test_external_flash(int argc, char **argv);
static void cli_update(int argc, char **argv);
static void cli_cloud_register(int argc, char **argv);

static const struct cli_command elight_base_cmds[] = { \
		{"dim", "(Set the lighting value)",						cli_dim_lighting},			\
		{"erase_nvm", "(Reset NVM Memory)",						cli_erase_nvm},				\
		{"reset_device", "(Reset Device)",						cli_reset_device},			\
		{"test_mfi", "(Test MFI Chip)",							cli_test_mfi},				\
		{"status", "(Report System Status)",					cli_system_status},			\
		{"test_flash", "(Test External Flash)",					cli_test_external_flash},	\
		{"trip", "(Trip the Alarm)",							cli_alarm_sim},	\
};

static const struct cli_command elight_wifi_cmds[] = { \
		{"connect", "(Connect to an available WiFi network)",	cli_wifi_connect},			\
		{"register", "(Save Cloud Provisioning Information)",	cli_cloud_register},		\
		{"update", "(Updates the Firmware)",					cli_update},				\
};

//*****************************************************************************
void register_commands()
{
	int i;
	for (i = 0; i < sizeof(elight_base_cmds) / sizeof(struct cli_command); i++)
		cli_register_command(&elight_base_cmds[i]);

	for (i = 0; i < sizeof(elight_wifi_cmds) / sizeof(struct cli_command); i++)
		cli_register_command(&elight_wifi_cmds[i]);
}

//*****************************************************************************
void initialize_uart()
{
	wmstdio_init(board_console_uart_id(), 0);
}

//*****************************************************************************
void cli_alarm_sim(int argc, char **argv)
{
	#if (CORDELIA_BOARD)
		motion_sensor_trip_cb();
	#else
		write_message("Command not supported");
	#endif
}

//*****************************************************************************
void cli_reset_device(int argc, char **argv)
{
	pm_reboot_soc();
}

//*****************************************************************************
void cli_erase_nvm(int argc, char **argv)
{
	reset_nvm_data();
}

//*****************************************************************************
void cli_test_mfi(int argc, char **argv)
{
	test_mfi();
}

//*****************************************************************************
void cli_dim_lighting(int argc, char **argv)
{
	if (argc == 2)
	{
		light_state_struct new_light_state = light_state;
		new_light_state.pulse_lighting	= false;
		new_light_state.power_state		= LIGHTS_ON;
		new_light_state.target_level	= atoi(argv[1]);
		new_light_state.pulse_speed		= 0;
		set_lighting(new_light_state);

	}
	else
		write_message("Command requires a single integer argument between 0 and 100");
}

//****************************************************************************
void cli_wifi_connect(int argc, char **argv)
{
	if (argc != 3)
	{
		write_message("Command requires 2 arguments: <SSID> <PassCode>");
		return;
	}
	char * ssid = argv[1];
	char * passphrase = argv[2];
	
	set_new_wifi_ssid(ssid);
	set_new_wifi_passphrase(passphrase);
	wifi_connect_to_new_network();
}

//****************************************************************************
void cli_system_status(int argc, char **argv)
{
	struct tm cur_time;
	wmtime_time_get(&cur_time);
	write_message("Firmware Version Info: %.02i.%.02i.%.02i", FW_VERSION_MAJOR, FW_VERSION_MINOR, FW_VERSION_SUBMINOR);
	write_message("Firmware Flavor: Prod %i, Dbg %i, MC200 %i, ExtAnt %i, Cord %i",	\
			PRODUCTION_BUILD, ENABLE_DEBUG, MC200_DEV_BOARD, EXTERNAL_ANTENNA, CORDELIA_BOARD);
	write_message("Firmware Build Time: " __DATE__ " " __TIME__ "");
	write_message("System Time:         %.04i-%.02i-%.02i at %.02i:%.02i:%.02i", 	\
		cur_time.tm_year + 1900, cur_time.tm_mon + 1, cur_time.tm_mday,				\
		cur_time.tm_hour, cur_time.tm_min, cur_time.tm_sec);

	float mcu_temp_c = read_mcu_temp();
	float mcu_temp_f = (mcu_temp_c * 9 / 5) + 32;
	write_message("Boot Count: %i - Uptime: %i", get_bootcount(), get_uptime());
	write_message("Internal MCU Temperature: %i C (%i F)", (int)mcu_temp_c, (int)mcu_temp_f);
	write_message("\tAC Frequency Measured: %i Hz True, %i Hz Locked", true_ac_power_frequency_hz, locked_ac_power_frequency_hz);
	if (light_state.pulse_lighting)
		write_message("\tLighting %s: Running Fading Demo", light_state.power_state ? "On" : "Off");
	else
		write_message("\tLighting %s: Fixed at %i", light_state.power_state ? "On" : "Off", light_state.target_level);
	write_message("\tTrue Dimming Range: %i to %i", LINEAR_RANGE_LO_POINT, LINEAR_RANGE_HI_POINT);

	write_message("\tWifi Status:\t%s", wifi_status_msg());
	write_message("\tCloud Status:\t%s", cloud_status_msg());

	report_scanned_ssids();
}

//****************************************************************************
void cli_test_external_flash(int argc, char **argv)
{
	test_external_flash();
}

//****************************************************************************
void cli_update(int argc, char **argv)
{
	if (argc != 2)
		write_message("Usage: update [network path to firmware file]");
	else if (strlen(argv[1]) > FW_UPDATE_PATH_MAX_LEN)
		write_message("Error: Firmware path too long (max %i).", FW_UPDATE_PATH_MAX_LEN - 1);
	else
		run_fw_update(argv[1]);
}

//****************************************************************************
void cli_cloud_register(int argc, char **argv)
{
	if (argc != 3)
	{
		write_message("Command requires 2 arguments: <API ID> <API Connection Token>");
		return;
	}
	char * api_id = argv[1];
	char * api_connection_token = argv[2];

	register_new_cloud_params(api_id, api_connection_token);
}
