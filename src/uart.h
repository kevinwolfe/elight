#ifndef _UART_H_
#define _UART_H_

	#include <wmstdio.h>
#include "main.h"
	
	void initialize_uart();

#if (ENABLE_DEBUG)
	#define write_message(_fmt_, ...)				\
		wmprintf("[elight] "_fmt_"\n\r", ##__VA_ARGS__)
#else
	#define write_message(_fmt_, ...)
#endif

	void register_commands();
	
#endif
