#ifndef _NVM_H__
#define _NVM_H__

	#include <stdint.h>
	#include <stdbool.h>
	
	#define NVM_NAME_BOOT_PARMAS		"nvm_boot_params"
	#define NVM_NAME_WIFI_PARAMS		"nvm_wifi_params"
	#define NVM_NAME_CLOUD_PARAMS		"nvm_cloud_params"
	#define NVM_NAME_LIGHTING_LEVELS	"nvm_lighting_levels"

	#define ENCRYPTED_STORE_MAGIC_KEY_LEN	(4)

	#define NVM_FAILURE		(false)
	#define NVM_SUCCESS		(true)

	void initialize_nvm();
	void reset_nvm_data();
	void test_external_flash();
	int get_bootcount();

	void factory_reset();

	bool read_nvm_value(const char * variable_name, void *value, uint32_t len);
	void write_nvm_value(const char * variable_name, const void *value, uint32_t len);
	bool read_nvm_encrypted_value(const char * variable_name, void *value, uint32_t len, void * magic_key_ptr);
	void write_nvm_encrypted_value(const char * variable_name, const void *value, uint32_t len, void * magic_key_ptr);


#endif
