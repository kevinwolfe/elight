#include "main.h"
#include <wm_os.h>
//#include <arch/sys.h>
#include <drivers/boot_flags.h>

#include "wifi_emb.h"
#include "timers_emb.h"
#include "ble.h"
#include "nvm.h"
#include "uart.h"
#include "update_fw.h"
#include "io.h"
#include "light_control.h"
#include "schedules.h"

// This is an entry point for the application.
int main(void)
{
	// Immediately pull DIM Control Low until we get Zero Crossings
	// Needs to be done first
	initialize_io();


	// Initialize Lowest level stuff first
#if (ENABLE_DEBUG)
	initialize_uart();
#endif

	initialize_timers();
	initialize_nvm();
	wm_core_and_wlan_init();

	// Do lighting after all level is complete as it affects user experience after turning the device on
	initialize_lighting();
	
	initialize_wifi();
	initialize_bluetooth();


	initialize_fw_updater();

#if (ENABLE_DEBUG)
	register_commands();
#endif

	initialize_schedules();

//	write_message("Reset Cause Register: 0x%x", boot_reset_cause());
	if (boot_reset_cause() & (1<<5))
		write_message("Watchdog hit!");

	int msec_count = 0;
	while (1)
	{
		check_ble_advertisement_state();

		watchdog_thread_checkin(MAIN_SYSTEM_THREAD);
		os_thread_sleep(os_msec_to_ticks(10));
		msec_count += 10;

		check_schedules();
		update_lighting_values_in_nvm();

		#if (LED_DRIVER)
			pwm_adjust_lighting();
			if (msec_count > 1000)
			{
				msec_count -= 1000;
				float adc_val = read_adc();

				write_message("ADC 0 Value: %d.%d mV", wm_int_part_of(adc_val), wm_frac_part_of(adc_val, 2));
			}
		#endif

	}

	return 0;
}

